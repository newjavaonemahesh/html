@extends('dashboard2')
@section('content')

<div class="panel panel-default">
  <div class="panel-heading">Summary</div>
  <div class="panel-body">
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            GMV:  {{$GMV}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Orders Received:  {{$orders->count()}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Orders Completed:  {{$completed->count()}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            In Transit:  {{$forwarded->count()}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Canceled:  {{$canceled->count()}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Pending:  {{$pending->count()}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Revenue:  {{$revenue}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Cost:  {{$cost}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Gross Profit:  {{$revenue-$cost}}
        </li>
      </ul>
    </div>
     <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            ABS Rs:  {{$ABS}}
        </li>
      </ul>
    </div>
    <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Avg SKU Rs:  {{$AIS}}
        </li>
      </ul>
    </div>
        <div class="col-sm-3">
      <ul class="list-group">
        <li class="list-group-item">
            Avg Orders/Day:  {{$AOPD}}
        </li>
      </ul>
    </div>
      
  </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">Cancellation Report (Total)</div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <td>Total</td><td>Out Of Stock</td><td>Customer Not Interested</td><td>Merchant Not Reachable</td>
                <td>Customer Not Reachable</td><td>No Coverage Area</td><td>Shipping Charges Too High</td>
                <td>Others</td>
            </thead>
            <tbody>
                <tr>
                    <td>{{$canceled->count()}}</td><td>{{$can_report['oos']}} %</td><td>{{$can_report['cni']}}%</td><td>{{$can_report['mnr']}}%</td>
                    <td>{{$can_report['cnr']}}%</td><td>{{$can_report['nca']}}%</td><td>{{$can_report['sth']}}%</td>
                    <td>{{$can_report['other']}}%</td>
                </tr>
                
            </tbody>
        </table>
        <br>
        <h3>This Months</h3>
        <br>
        <table class="table">
            <thead>
                <td>Total</td><td>Out Of Stock</td><td>Customer Not Interested</td><td>Merchant Not Reachable</td>
                <td>Customer Not Reachable</td><td>No Coverage Area</td><td>Shipping Charges Too High</td>
                <td>Others</td>
            </thead>
            <tbody>
                <tr>
                    <td>{{$can_report_m['total']}}</td><td>{{$can_report_m['oos']}} %</td><td>{{$can_report_m['cni']}}%</td><td>{{$can_report_m['mnr']}}%</td>
                    <td>{{$can_report_m['cnr']}}%</td><td>{{$can_report_m['nca']}}%</td><td>{{$can_report_m['sth']}}%</td>
                    <td>{{$can_report_m['other']}}%</td>
                </tr>
                
            </tbody>
        </table>
    </div>
</div>
<div class="panel panel-primary">
    <div class="panel-heading">Orders By City</div>
    <div class="panel-body">
         <div class="col-sm-6">
             <div class="panel panel-default">
             <div class="panel-heading">City Distribution Report. Order Received from {{sizeof($cities)}} unique cities</div>
             <div class="panel-body">
                @foreach($cities as $city)
                    <span class="col-sm-5">{{$city['name']}}</span><span class="col-sm-1">{{$city['orders']}}</span><span class="col-sm-1">{{$city['percentage']}}%</span> <br>
                @endforeach                
             </div>
             </div>
        
         </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">Courier Report</div>
                <div class="panel-body">
                    <table class="table">
                <thead>
                    <tr>
                        <th>Courier</th>
                        <th>Orders</th>
                        <th>Cost</th>
                        <th>Avg Time</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>BlueEx</td>
                        <td>1230</td>
                        <td>52,097</td>
                        <td>8</td>
                    </tr>
                    <tr>
                        <td>Leopards</td>
                        <td>123</td>
                        <td>14,097</td>
                        <td>4</td>
                    </tr>
                </tbody>
            </table>
                </div>
            </div>
        </div>
        
    </div>
</div>
<meta name="_token" content="{!! csrf_token() !!}" />
@stop