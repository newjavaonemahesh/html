
@extends('dashboard2')
@section('content')
@if($createForm == true)
    <div class="panel panel-default missing-merchant">
        <div class="panel-heading">{{$name}}, Configuration Missing</div>
        <form class="form-inline" role="form">
            <div class="row">
                <div class="col-sm-3 form-group">
                    <label>Name</label>
                    <input class="form-control merchant-name" type="text" value="{{$name}}"/>
                </div>
                <div class="col-sm-5 form-group">
                    <label>Brand Name</label>
                    <input class="form-control merchant-brand" type="text" value="{{$name}}"/>
                </div>
                <div class="col-sm-4 form-group">
                    <label>Commission</label>
                    <input class="form-control merchant-commission" type="text" value="15"/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 form-group">
                    <label>Email</label>
                    <input class="form-control merchant-email" type="text" value=""/>
                </div>
                <div class="col-sm-3 form-group">
                    <label>Phone Number</label>
                    <input class="form-control merchant-phone" type="text" value=""/>
                </div>
                 <div class="col-sm-3 form-group">
                    <label>City</label>
                    <input class="form-control merchant-city" type="text" value=""/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 form-group">
                    <label>Address</label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <textarea class="merchant-address" rows="3" cols="50 "name="address" form="usrform">Enter address here...</textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-default btn-merchant-add">ADD</button>
        </form>
    </div>
@endif

<div class="panel panel-default">
  <div class="panel-heading">{{$name}}, Dashboard</div>
  <div class="panel-body">
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$dashboard['orders']}}</span>
            Orders
        </li>
        <li class="list-group-item">
          <span class="badge">{{$dashboard['delivered']}}</span>
            Delivered
        </li>
        <li class="list-group-item">
          <span class="badge">{{$dashboard['canceled']}}</span>
            Canceled
        </li>
        <li class="list-group-item">
          <span class="badge">0</span>
            KPI Breached
        </li>
      </ul>
    </div>
    <div class="col-sm-4">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$dashboard['sales']}}</span>
            Sales
        </li>
        <li class="list-group-item">
          <span class="badge">{{$dashboard['revenue']}}</span>
            Revenue
        </li>
        <li class="list-group-item">
          <span class="badge">{{$dashboard['paymentDue']}}</span>
            Payment Due
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Orders</div>
  <div class="panel-body">
    <table class="table items-table">
      <thead>
          <tr>
                <th>Order ID</th>
                <th>Status</th>
                <th>Received On</th>
                <th>Order Amount</th>
                <th>Merchant Name</th>
                <th>Order City</th>
                <th>Courier</th>
          </tr>
      </thead>
      <tbody>
          @foreach($orders as $order)
            <tr>
                <td>
                    <a href="/order/{{$order->order_id}}"><span>{{$order->order_id}}</span></a>
                </td>
                <td>{{$order->order_status}}</td>
                <td>{{$order->create_time}}</td>
                <td>{{$order->grand_amount}}</td>
                <td>
                    <a href="/merchants/{{$order->merchant}}"><span>{{$order->merchant}}</span></a>
                </td>
                <td>{{$order->city}}</td>
                @if($order->courier!=null)
                    <td>{{$order->courier}}</td>
                @else
                <td>--</td>
                @endif
            </tr>
            @endforeach
      </tbody>
    </table>
  </div>
</div>

<meta name="_token" content="{!! csrf_token() !!}" />

@stop

