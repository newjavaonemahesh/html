@extends('dashboard2')
@section('content')
 
    <form action="/upload" method="post" enctype="multipart/form-data">
        <input type="file" name="filefield">
        <input type="submit">
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
    </form>
 
<br>
@if(isset($total_records))
<p>Records Processed: {{$total_records}}</p>
<br>
<h4>Duplicates</h4>
@foreach($duplicate as $order)
{{$order}}
,
 @endforeach
 <h4>New Orders</h4>
 @foreach($new as $order)
{{$order}}
,
 @endforeach
 <h4>Edited Orders</h4>
 @foreach($edited as $order)
{{$order}}
,
 @endforeach
 <h4>Orders Moved to History</h4>
 @foreach($archived as $order)
{{$order}}
,
 @endforeach
 
 <h3>Skipped Orders. Correct and reload</h3>
 @foreach($skipped as $order)
{{$order}}
<br>
 @endforeach
 
 @endif
 
<meta name="_token" content="{!! csrf_token() !!}" />
@endsection

