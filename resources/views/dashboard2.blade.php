<!doctype html>
<html>
<head>
    @include('head')
</head>
<body>
<div class="container-fluid">
@include('header')
@include('common.flash')
@yield('content')
@include('footer')
</div>
</body>
</html>
