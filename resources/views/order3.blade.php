@extends('dashboard2')
@section('content')

<div class="panel panel-primary">
  <div class="panel-heading master-order" id='{{$order->order_id}}'>{{$order->order_id}} | {{$order->merchant}}</div>
  <div class="panel-body">
    <table class="table">
      <thead>
        <tr>
                        <th>Status</th>
                        <th>Received on</th>
                        <th>Order Amount</th>
                        <th>Order City</th>
                        <th>Courier</th>
                        <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
              <select id="{{$order->order_id}}">
                <option value="{{$order->order_status}}">{{$order->order_status}}</option>
                <option value="Received">Received</option>
                <option value="Pending">Pending</option>
                <option value="Forwarded">Forwarded</option>
                <option value="Dispatched">Dispatched</option>
                <option value="Delivered">Delivered</option>
                <option value="Canceled">Canceled</option>
                <option value="Returned">Returned</option>
              </select>
          </td>
          <td>{{$order->create_time }}</td>
          <td>{{$order->grand_amount}}</td>
          <td>{{$order->city}}</td>
          <td>{{$order->courier}}</td>
          <td><button type="button" value="{{$order->order_id}}" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-status-update">Update</button></td>
        </tr>
        <tr>
          <td>Customer confirmed</td><td>@if($order->customer_confirm != null){{$order->customer_confirm}}@else <button type="button" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-customer-confirm">Customer Confirm</button> @endif</td>
        </tr>
        <tr>
          <td>Merchant confirmed</td><td>@if($order->merchant_confirm != null){{$order->merchant_confirm}}@else <button type="button" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-merchant-confirm">Merchant Confirm</button> @endif</td>
        </tr>
      </tbody>
      </table>
    </div>
  </div>

<div class="panel panel-default">
  <div class="panel-heading">SKU Details</div>
  <div class="panel-body">
    <table class="table items-table" value="{{$order->merchant}}" id="{{$order->merchant}}">
      <thead>
          <tr>
                <th>SKU</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>List Price</th>
                <th>Sale Price</th>
                <th>Promotion</th>
                <th>Promotion Discount</th>
                <th>Total Price</th>
                <th>Merchant Payable</th>
          </tr>
      </thead>
      <tbody>
          @foreach($items as $item)
          <tr class='item-row'>
              <td class='sku'>{{$item->item_sku}}</td>
              <td class='name'>{{$item->item_name}}</td>
              <td class='quantity'>{{$item->item_quantity}}</td>
              <td class='list-price'>{{$item->item_list_price}}</td>
              <td class='sale-price'>{{$item->item_sale_price}}</td>
              <td class='promotion'>{{{$item->promotion_name or '--'}}}</td>
              <td class='discount'>{{{$item->promotion_discount or '--'}}}</td>
              <td class='grand-price'>{{$item->items_grand_price}}</td>
              <td class='merchant-payable'>{{$item->items_merchant_payable}}</td>
          </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr>
              <td>
              <button type="button" value="{{$order->order_id}}" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-items-edit">Edit</button>
              </td>
          </tr>
      </tfoot>
    </table>
  </div>
  </div>
<div class="panel panel-default">
  <div class="panel-heading">Shipping Details</div>
  <div class="panel-body">
    <table class="table">
      <thead>
          <tr>
            <th>Courier</th>
            <th>CN</th>
            <th>Destination</th>
            <th>Forwarded</th>
            <th>Dispatched</th>
            <th>Delivered</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>
                  @if($order->courier != null){{$order->courier}} 
                  @else <select id="courier_company">
                            <option value="NotSelected">Select Courier</option>
                            <option value="BluEx">BluEx</option>
                            <option value="Leopard">Leopard</option>
                            <option value="Clikky">CLIKKY</option>
                        </select>
                  @endif
              </td>
              <td> <span>@if($order->CN != null){{ $order->CN }} @else <a href="#1" data-pk="{{$order->order_id}}" id="updateCN">Enter CN </a> @endif</span></td>
              <td>{{ $order->city }}</td>
              <td>@if($order->forward_time !=null) {{$order->forward_time }} @else -- @endif</td>
              <td>@if($order->dispatch_time !=null) {{$order->dispatch_time }} @else -- @endif</td>
              <td>@if($order->deliver_time !=null) {{$order->deliver_time }} @else -- @endif</td>
              <td>
                  @if($order->CN == null) 
                    <button type="button" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-generate-cn">Generate CN</button>
                  @else
                    <button type="button" class="btn btn-warning btn-xs" disabled="">Generate CN</button>
                  @endif
              </td>
          </tr>
      </tbody>
      
    </table>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Customer Details</div>
  <div class="panel-body">
    <table class="table">
      <thead>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Email</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>{{$order->name}}</td>
              <td>{{$order->address}}</td>
              <td>{{$order->phone}}</td>
              <td>{{$order->email}}</td>
          </tr>
      </tbody>
      
    </table>
  </div>
</div>
@if($slave !=null)
<div class="panel panel-default">
  <div class="panel-heading">
      <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapse1">+Previous Order</a>
      </h4>
  </div>
  <div class="panel-body collapse" id="collapse1">
    <table class="table">
      <thead>
          <tr>
            <th>Order ID</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>{{$slave->order_id}}</td>
          </tr>
      </tbody>
      
    </table>
  </div>
</div>
@endif



            <!-- End of Table-to-load-the-data Part -->
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 80%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Merchant Items</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
                                <table class='table item-edit-form'>
                                    <thead>
                                        <tr>
                                            <th>SKU</th>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Sale Price</th>
                                            <th>Merchant Payable</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-merchant-pay" id="btn-save" value="add">Update</button>
                            <input type="hidden" id="task_id" name="task_id" value="0">
                        </div>
                    </div>
                </div>
            </div>


<meta name="_token" content="{!! csrf_token() !!}" />

@stop
