@extends('dashboard2')
@section('content')
<script>
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>    
<div class="panel panel-default">
  <div class="panel-heading">Dashboard</div>
  <div class="panel-body">
      <div class="row">
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$all}}</span>
            <a href="#" id="getAllOrders"><span>ALL</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$completed}}</span>
            <a href="#" id="getCompletedOrders"><span>Completed</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$canceled}}</span>
            <a href="#" id="getCanceledOrders"><span>Canceled</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$critical}}</span>
            <a href="#" id="getCriticalOrders"><span class="label label-danger">CRITICAL</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$serious}}</span>
            <a href="#" id="getSeriousOrders"><span class="label label-warning">SERIOUS</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$warning}}</span>
          <a href="#" id="getWarningOrders"><span class="label label-info">WARNING</span></a>
        </li>
      </ul>
    </div>
  </div>
  </div>
  <div class="row">
      <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$received}}</span>
          <a href="#" id="getReceivedOrders"><span>Received</span></a>
        </li>
      </ul>
    </div>
      <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$forwarded}}</span>
          <a href="#" id="getForwardedOrders"><span>Forwarded</span></a>
        </li>
      </ul>
    </div>
      <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$dispatched}}</span>
          <a href="#" id="getDispatchedOrders"><span>Dispatched</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$pending}}</span>
          <a href="#" id="getPendingOrders"><span>Pending</span></a>
        </li>
      </ul>
    </div>
      <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$confirmed}}</span>
          <a href="#" id="getWaitingOrders"><span>Confirmed</span></a>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            
            
                           <div id="reportrange" class="pull-left" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 20%">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                <span></span> <b class="caret"></b>
                            </div>

                            <script type="text/javascript">
                            $(function() {

                                function cb(start, end) {
                                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                                }
                                cb(moment().subtract(29, 'days'), moment());

                                $('#reportrange').daterangepicker({
                                    ranges: {
                                       'Today': [moment(), moment()],
                                       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                       'This Month': [moment().startOf('month'), moment().endOf('month')],
                                       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                    }
                                }, cb);

                            });
                            </script>
        <label text="Order Status" id="order-status-">Order Status:</label>
        <select id="order-status-dropdown">
            <option>Any</option>
            <option>Received</option>
            <option>Confirmed</option>
            <option>Pending</option>
            <option>Canceled</option>
            <option>Forwarded</option>
            <option>Dispatched</option>
            <option>Returned</option>
            <option>Complete</option>
            <option>Delivered</option>
            <option>PayableNotSet</option>
        </select>
        <label text="Order Status" id="order-status-">Order ID:</label>
        <input type="text" id="search-order-id-text" class="">
        <label text="Order Status" id="search-order-name-label">Customer Name:</label>
        <input type="text" class="" id="search-order-name-text">
        <label text="Order Status" id="search-order-phone-label">Customer Phone:</label>
        <input type="text" class="" id="search-order-phone-text">
        <input type="button" class="btn btn-primary btn-sm" id="btn-search-order" value="Search">
	</div>
  
        
    </div>  
<div class="panel panel-default">
    <div class="panel-heading">Customer Orders 
        <div class="" style="text-align: right;">
            <!--<input type="button" class="btn btn-success" onclick="printDiv('printableArea')" value="Print Report" />-->
        </div>
    </div>
  <div class="panel-body">
      <div id="printableArea">
    <table class="table">
        <thead>
            <tr>
                <th>Order ID</th>
                <th>Status</th>
                <th>Received On</th>
                <th>Dispatched On</th>
                <th>Order Amount</th>
                <th>Merchant Name</th>
                <th>Order City</th>
                <th>Courier</th>
                <th>CN</th>
            </tr>
        </thead>
        <tbody id="orders_data">
            
            @foreach(array_chunk($orders->all(),20) as $page_orders)
            @foreach($page_orders as $order)
            <tr @foreach($order->items as $item)
                 @if($item->items_merchant_payable == NULL)
                    class="bg-danger"
                 @endif
                 @endforeach
                 
                 >
                <td>
                    <a href="/order/{{$order->order_id}}"><span>{{$order->order_id}}</span></a>
                </td>
                <td>{{$order->order_status}}</td>
                <td>{{$order->create_time}}</td>
                <td>{{$order->dispatch_time}}</td>
                <td>{{$order->grand_amount}}</td>
                <td>
                    <a href="/merchants/{{$order->merchant}}"><span>{{$order->merchant}}</span></a>
                </td>
                <td>{{$order->city}}</td>
                @if($order->courier!=null)
                    <td>{{$order->courier}}</td>
                @else
                <td>--</td>
                @endif
                @if($order->CN!=null)
                    <td>{{$order->CN}}</td>
                @else
                <td>-</td>
                @endif
                
            </tr>
            <tr>
                <td colspan="12" width="100%">
                    
                    
                    <!---<table class="table">
                        <tr>
                            <th width="10%" class="text-center">Order No</th>
                            <th width="10%" class="text-center">Customer Name</th>
                            <th width="10%" class="text-center">Phone #</th>
                            <th width="40%" class="text-center">Address</th>
                            <th width="50%" class="text-center">Item Name</th>
                        </tr>

                        <tr>
                            <td width="10%" class="text-center">{{$order->id }}</td>
                            <td width="10%" class="text-center">{{$order->name }}</td>
                            <td width="10%" class="text-center">{{$order->phone}}</td>
                            <td width="20%" class="text-center">{{$order->address}}</td>
                            <td width="50%" class="text-center">@foreach($order->items as $item)
                                
                                {{$item->item_name}},<br>
                                
                                @endforeach
                            </td>
                        </tr>

                    </table>--->
                </td>
            </tr>
            @endforeach
            @endforeach
            
        </tbody>
    </table>
    
    </div>
      
      <div id="pagination">
                {{$orders->links()}}
      </div>
      </div>
      </div>
      </div>
<meta name="_token" content="{!! csrf_token() !!}" />
@stop