<!doctype html>
<html>
<head>
    <link href="{{{ asset('/css/master.css') }}}" rel="stylesheet">
    @include('head')
</head>
<body>
<div class="container">

    <header class="row">
        @include('header')
    </header>

    <div id="main" class="row">

            <!-- sidebar content -->
        <div id="sidebar" class="col-md-4">
            @include('sidebar')
        </div>
        <!-- main content -->
        <div id="content" class="col-md-8">
            @yield('content')
        </div>

    </div>

    <footer class="row">
        @include('footer')
    </footer>

</div>
</body>
</html>