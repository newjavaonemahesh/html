@extends('dashboard2')
@section('content')
<div class="panel panel-default">
  <div class="panel-heading">Dashboard</div>
  <div class="panel-body">
      <div class="row">
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$all}}</span>
            <a href="#" id="getAllOrders"><span>ALL</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$critical}}</span>
            <a href="#" id="getCriticalOrders"><span class="label label-danger">CRITICAL</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$serious}}</span>
            <a href="#" id="getSeriousOrders"><span class="label label-warning">SERIOUS</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-2">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$warning}}</span>
          <a href="#" id="getWarningOrders"><span class="label label-info">WARNING</span></a>
        </li>
      </ul>
    </div>
  </div>
  </div>
  <div class="row">
      <div class="col-sm-1">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$received}}</span>
          <a href="#" id="getReceivedOrders"><span>Received</span></a>
        </li>
      </ul>
    </div>
      <div class="col-sm-1">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$forwarded}}</span>
          <a href="#" id="getForwardedOrders"><span>Forwarded</span></a>
        </li>
      </ul>
    </div>
      <div class="col-sm-1">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$dispatched}}</span>
          <a href="#" id="getDispatchedOrders"><span>Dispatched</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-1">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$pending}}</span>
          <a href="#" id="getPendingOrders"><span>Pending</span></a>
        </li>
      </ul>
    </div>
      <div class="col-sm-1">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$waiting_confirm}}</span>
          <a href="#" id="getWaitingOrders"><span>WCC</span></a>
        </li>
      </ul>
    </div>
    <div class="col-sm-1">
      <ul class="list-group">
        <li class="list-group-item">
          <span class="badge">{{$waiting_confirm}}</span>
          <a href="#" id="getWaitingOrders"><span>WMC</span></a>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
        <div id="filter-panel-order" class="collapse filter-panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Order ID:</label>
                            <input type="text" class="form-control input-sm" id="search-order-id-text">
                        </div><!-- form group [search] -->
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="btn-search-order">Search</button>                               
                        </div> <!-- form group [order by] --> 
                    </form>
                </div>
            </div>
        </div>
        <div id="filter-panel-customer" class="collapse filter-panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form">
                        <div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Name:</label>
                            <input type="text" class="form-control input-sm" id="search-order-name-text">
                        </div><!-- form group [search] -->
                        <div class="form-group">
                            <label class="filter-col" style="margin-right:0;" for="pref-search">Phone #:</label>
                            <input type="text" class="form-control input-sm" id="search-order-phone-text">
                        </div><!-- form group [search] -->
                        <div class="form-group">
                            <button type="button" class="btn btn-primary" id="btn-search-customer">Search</button>                               
                        </div> <!
                        <!-- form group [order by] --> 
                    </form>
                </div>
            </div>
        </div>

         <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#filter-panel-order">
            <span class="glyphicon glyphicon-cog"></span> Search Order
        </button>
        <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#filter-panel-customer">
            <span class="glyphicon glyphicon-cog"></span> Search Customer
        </button>
	</div>
  
        
    </div>  
<div class="panel panel-default">
  <div class="panel-heading">Customer Orders</div>
  <div class="panel-body">
    <table class="table">
        <thead>
            <tr>
                <th>Order ID</th>
                <th>Status</th>
                <th>Received On</th>
                <th>Dispatched On</th>
                <th>Order Amount</th>
                <th>Merchant Name</th>
                <th>Order City</th>
                <th>Courier</th>
                <th>CN</th>
            </tr>
        </thead>
        <tbody id="orders_data">
            
            @foreach(array_chunk($orders->all(),20) as $page_orders)
            @foreach($page_orders as $order)
            <tr>
                <td>
                    <a href="/order/{{$order->order_id}}"><span>{{$order->order_id}}</span></a>
                </td>
                <td>{{$order->order_status}}</td>
                <td>{{$order->create_time}}</td>
                <td>{{$order->dispatch_time}}</td>
                <td>{{$order->grand_amount}}</td>
                <td>
                    <a href="/merchants/{{$order->merchant}}"><span>{{$order->merchant}}</span></a>
                </td>
                <td>{{$order->city}}</td>
                @if($order->courier!=null)
                    <td>{{$order->courier}}</td>
                @else
                <td>--</td>
                @endif
                @if($order->CN!=null)
                    <td>{{$order->CN}}</td>
                @else
                <td>-</td>
                @endif
            </tr>
            @endforeach
            @endforeach
            
        </tbody>
    </table>
                {{$orders->links()}}
      </div>
      </div>
      </div>
<meta name="_token" content="{!! csrf_token() !!}" />
@stop