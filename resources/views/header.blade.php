
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">HOME</a>
    </div>
@if (!Auth::guest())
                        
                   
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="/reports">Reports</a></li>
        <!---umer -->
        <li class="dropdown">
            <a href="#1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Merchant Bills<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="/pay_reports">Generate Bills</a></li>
                <li><a href="/paid_reports">Bill History</a></li>
          </ul>
        </li>
        
        <!-- umer--->
        
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="#1">Settings</a></li>
        <li class="dropdown">
          <a href="#1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="add-merchant">Add Merchant</a></li>
            <li><a href="/upload">Upload File</a></li>
            <li><a href="#1">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
            <li><a href="/logout">Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
     @endif
  </div><!-- /.container-fluid -->
</nav>