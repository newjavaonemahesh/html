@extends('dashboard2')
@section('content')
<div class="panel panel-primary">
  <div class="panel-heading master-order">Merchant Payments</div>
  <div class="panel-body">
      <div>
          <select id="merchant-list">
              <option value="Received">Select Merchant</option>
          </select>
      </div>
      <div class="panel-body ">
          <div class="panel-body col-sm-1 bg-info">
              CLK12321
          </div>
          <div class="panel-body col-sm-11">
              <div class="row">
                  <div class="col-sm-1">SKU</div> <div class="col-sm-1">Title</div> <div class="col-sm-1">Sale Price</div>
              </div>
              <div class="row">
                  Hello Row2
              </div>
          </div>
      </div>
      <table class='table'>
          <thead>
              <tr>
                        <th>Order ID</th>
                        <th>Received on</th>
                        <th>Delivered On</th>
                        <th>Order Amount</th>
                        <th>Order Merchant Payable</th>
              </tr>
          </thead>
          <tbody class="merchant-payable-items">
              <tr>
                  <td>CLK1111</td>
                  <td>23-12-2938</td>
                  <td>23-12-2938</td>
                  <td>1000</td>
                  <td>900</td>
              </tr>
              <tr>
                  <td colspan="1"></td>
                  <td colspan="4">
                      <div class="panel panel-body">
                          ITEM SKU DETAILs HERE
                      </div>
                  </td>
              </tr>
          </tbody>
      </table>
    

    </div>
  </div>
<div class="panel panel-primary">
  <div class="panel-heading master-order">Recent Merchant Payments</div>
  <div class="panel-body">
      <table class='table'>
          <thead>
              <tr>
                        <th>Merchant</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Payment ID</th>
              </tr>
          </thead>
          <tbody class="merchant-payment-history">
              @foreach($payments as $payment)
              <tr>
                  <td> {{$payment->merchantName}}</td>
                  <td> {{$payment->amount}}</td>
                  <td> {{$payment->updated_at}}</td>
                  <td> {{$payment->id}}</td>
              </tr>
               @endforeach
          </tbody>
      </table>
    

    </div>
  </div>
<meta name="_token" content="{!! csrf_token() !!}" />
@stop

