@extends('dashboard2')
@section('content')

<div class="panel panel-primary">
  <div class="panel-heading"><?php echo $order->order_id ?></div>
  <div class="panel-body">
    <div class="table">
      <div class="row">
        <div class="col-sm-1">
          Order ID
        </div>
        <div class="col-sm-2">
          Status
        </div>
        <div class="col-sm-2">
          Received on
        </div>
        <div class="col-sm-1">
          Order Amount
        </div>
        <div class="col-sm-1">
          Merchant Name
        </div>
        <div class="col-sm-1">
          Order City
        </div>
        <div class="col-sm-1">
          Courier
        </div>
        <div class="col-sm-3">
          Action 
        </div>
      </div>
      <div class="row {{$order->order_id }}">
          <?php
          echo '<div class="col-sm-1">' . $order->order_id .'</div>';
          ?>
          <div class="col-sm-2">
              <select id="{{$order->order_id}}">
                <option value="{{$order->order_status}}">{{$order->order_status}}</option>
                <option value="Received">Received</option>
                <option value="Acknowledged">Acknowledged</option>
                <option value="CustomerConf">Customer Confirmed</option>
                <option value="MerchantConf">Merchant Confirmed</option>
                <option value="Forwarded">Forwarded</option>
                <option value="Dispatched">Dispatched</option>
                <option value="Delivered">Delivered</option>
                <option value="Canceled">Canceled</option>
                <option value="Returned">Returned</option>
              </select>
          </div>    

            <?php
          //echo '<div class="col-sm-1">' . $order->order_status . '</div>';
          echo '<div class="col-sm-2">' . $order->create_time . '</div>';
          echo '<div class="col-sm-1">' . $order->grand_amount . '</div>';
          echo '<div class="col-sm-1">' . $order->merchant . '</div>';
          echo '<div class="col-sm-1">' . $order->city . '</div>';
          echo '<div class="col-sm-1">' . $order->courier . '</div>';
          echo '<div class="col-sm-3">';?>
          <button type="button" value="{{$order->order_id}}" id="{{$order->order_id}}" class="btn btn-warning btn-xs">Update</button>
        </div>
        <div class="row">
          <div class="col-sm-2">Merchant Confirmed</div>
          <div class="col-sm-2">NO      <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></div>
        </div>
        <div class="row">
          <div class="col-sm-2">Customer Confirmed  </div>
          <div class="col-sm-2">Yes      <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">SKU Details</div>
  <div class="panel-body">
    <div class="table">
      <div class="row">
        <div class="col-sm-1">SKU</div>
        <div class="col-sm-4">Product Name</div>
        <div class="col-sm-1">Quantity</div>
        <div class="col-sm-1">List Price</div>
        <div class="col-sm-1">Sale Price</div>
        <div class="col-sm-1">Promotion</div>
        <div class="col-sm-1">Promotion Discount</div>
        <div class="col-sm-1">Total Price</div>
        <div class="col-sm-1">Merchant Payble</div>
      </div>
      <?php foreach ($items as $item) {
        echo '<div class="row">';
            echo '<div class="col-sm-1">' . $item->item_sku . '</div>';
            echo '<div class="col-sm-4">' . $item->item_name . '</div>';
            echo '<div class="col-sm-1">' . $item->item_quantity . '</div>';
            echo '<div class="col-sm-1">' . $item->item_list_price . '</div>';
            echo '<div class="col-sm-1">' . $item->item_sale_price . '</div>';
            echo '<div class="col-sm-1">' . $item->promotion_name . '</div>';
            echo '<div class="col-sm-1">' . $item->promotion_discount . '</div>';
            echo '<div class="col-sm-1">' . $item->items_grand_price . '</div>';
            echo '<div class="col-sm-1">' . $item->items_merchant_payable . '</div>';
        echo '</div>';
      }
      ?>
    </div>
  </div>
  </div>
<div class="panel panel-default">
  <div class="panel-heading">Shipping Details</div>
  <div class="panel-body">
    <div class="table">
      <div class="row">
        <div class="col-sm-1">Courier</div>
        <div class="col-sm-1">CN</div>
        <div class="col-sm-1">Destination</div>
        <div class="col-sm-2">Forwarded</div>
        <div class="col-sm-2">Dispatched</div>
        <div class="col-sm-2">Delivered</div>
      </div>
      
      <div class="row">
          <div class="col-sm-1"> {{$order->courier}} </div>
            <div class="col-sm-1"> {{ $order->CN }}</div>
           <div class="col-sm-1">{{ $order->city }}</div>
            <div class="col-sm-2"> @if($order->forward_time !=null) {{$order->forward_time }} @else -- @endif</div>
           <div class="col-sm-2"> @if($order->dispatch_time !=null) {{$order->dispatch_time }} @else -- @endif</div>
          <div class="col-sm-2">@if($order->deliver_time !=null) {{$order->deliver_time }} @else -- @endif</div>
          <div class="col-sm-1">
            @if($order->CN == null) 
              <button type="button" value="cn" id="cn" class="btn btn-warning btn-xs">Generate CN</button>
            @else
              <button type="button" value="cn" id="cn" class="btn btn-warning btn-xs" disabled="">Generate CN</button>
            @endif
          </div>
      </div>
      
      
    </div>
  </div>
</div>

<meta name="_token" content="{!! csrf_token() !!}" />

@stop

