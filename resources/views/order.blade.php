@extends('dashboard2')
@section('content')

<div class="panel panel-primary">
  <div class="panel-heading"><?php echo $order->order_id ?></div>
  <div class="panel-body">
    <div class="table">
      <div class="row">
        <div class="col-sm-1">
          Order ID
        </div>
        <div class="col-sm-1">
          Status
        </div>
        <div class="col-sm-2">
          Received on
        </div>
        <div class="col-sm-1">
          Order Amount
        </div>
        <div class="col-sm-1">
          Merchant Name
        </div>
        <div class="col-sm-1">
          Order City
        </div>
        <div class="col-sm-1">
          Courier
        </div>
        <div class="col-sm-3">
          Action
        </div>

      </div>
      <div class="row">
          <?php
          echo '<div class="col-sm-1">' . $order->order_id .'</div>';
          /*
          <div class="dropdown">
              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Dropdown
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
          */

          echo '<div class="col-sm-1">' . $order->order_status . '</div>';
          echo '<div class="col-sm-2">' . $order->create_time . '</div>';
          echo '<div class="col-sm-1">' . $order->grand_amount . '</div>';
          echo '<div class="col-sm-1">' . $order->merchant . '</div>';
          echo '<div class="col-sm-1">' . $order->city . '</div>';
          echo '<div class="col-sm-1">' . $order->courier . '</div>';
              echo '<div class="col-sm-3"> 
                  <button onClick.load( "ajax/test.html") type="button" class="btn btn-primary">Update</button>

             </div>';
        ?>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">SKU Details</div>
  <div class="panel-body">
    <div class="table">
      <div class="row">
        <div class="col-sm-1">SKU</div>
        <div class="col-sm-4">Product Name</div>
        <div class="col-sm-1">Quantity</div>
        <div class="col-sm-1">List Price</div>
        <div class="col-sm-1">Sale Price</div>
        <div class="col-sm-1">Promotion</div>
        <div class="col-sm-1">Promotion Discount</div>
        <div class="col-sm-1">Total Price</div>
        <div class="col-sm-1">Merchant Payble</div>
      </div>
      <?php foreach ($items as $item) {
        echo '<div class="row">';
            echo '<div class="col-sm-1">' . $item->item_sku . '</div>';
            echo '<div class="col-sm-4">' . $item->item_name . '</div>';
            echo '<div class="col-sm-1">' . $item->item_quantity . '</div>';
            echo '<div class="col-sm-1">' . $item->item_list_price . '</div>';
            echo '<div class="col-sm-1">' . $item->item_sale_price . '</div>';
            echo '<div class="col-sm-1">' . $item->promotion_name . '</div>';
            echo '<div class="col-sm-1">' . $item->promotion_discount . '</div>';
            echo '<div class="col-sm-1">' . $item->items_grand_price . '</div>';
            echo '<div class="col-sm-1">' . $item->items_merchant_payable . '</div>';
        echo '</div>';
      }
      ?>
    </div>
  </div>
  </div>
<div class="panel panel-default">
  <div class="panel-heading">Shipping Details</div>
  <div class="panel-body">
    <div class="table">
      <div class="row">
        <div class="col-sm-1">Courier</div>
        <div class="col-sm-1">CN</div>
        <div class="col-sm-1">Destination</div>
        <div class="col-sm-1">Forwarded</div>
        <div class="col-sm-1">Dispatched</div>
        <div class="col-sm-1">Delivered</div>
      </div>
      <?php
        echo '<div class="row">';
            echo '<div class="col-sm-1">' . $order->courier . '</div>';
            echo '<div class="col-sm-1">' . $order->CN . '</div>';
            echo '<div class="col-sm-1">' . $order->city . '</div>';
            echo '<div class="col-sm-1">' . $order->forwarded_time . '</div>';
            echo '<div class="col-sm-1">' . $order->dispatched_time . '</div>';
            echo '<div class="col-sm-1">' . $order->delivered_time . '</div>';
        echo '</div>';
      ?>
    </div>
  </div>
</div>

@stop

