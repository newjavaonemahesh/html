@if(Session::has('msg'))
<div class='alert {{Session::get('alert_class')}}' role='alert'>
    <strong>Info:</strong> {{ Session::get('msg')}}
</div>
@endif
