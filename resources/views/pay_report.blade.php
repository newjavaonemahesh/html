@extends('dashboard2')
@section('content')
<script>
    $(document).ready(function(){
        $( "#merchant" ).change(function() {
//            alert( $(this).val());
            $("#result").hide();
            var data = $(this).val();
            
            $.ajax({
        type:"GET",
        cache:false,
        url:"/merchant_report",
        data:{merchant:data},    // multiple data sent using ajax
        success: function (html) {
            $("#report").show();
            $("#result").show();
            $("#result").html(html);
            }
            });
            $("#merchant_name").val(data);
            $("label#mer").text(data);
            $("input#mer").val(data);
        });
        
        $("#submit").click(function(){
            var mername = $("#merchant_name").val();
            $.ajax({
        type:"GET",
        cache:false,
        url:"/merchant_report_download",
        data:{merchant:mername},    // multiple data sent using ajax
        success: function (html) {
            alert(html);
            }
            });
        });
        
        $("#make_payment").click(function(){
            
            var str = $( "#frm_paymet" ).serialize();
            //alert(str);
        $.ajax({
        type:"GET",
        cache:false,
        url:"/make_payment",
        data:str,    // multiple data sent using ajax
        success: function (html) {
            alert(html);
            }
            });
        });
        function check()
        {
     //var val=(this).val(); // it will get value from checked checkbox;
    alert('val');
        }

        
    });
    
    function printDiv() {
     var printContents = document.getElementById("print").innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
                        }
    
    
</script>
<div class="panel panel-default">
    <div class="panel-heading master-order" style=" background-color: #337ab7; background: #337ab7; color: #fff;"><h3>Merchant Selection</h3></div>
  <div class="panel-body">
      <form class="form-inline" role="form">
          <div class="row">
                <div class="col-sm-12 form-group">
                    <label><h3>Select a Merchant</h3></label>&nbsp;&nbsp;&nbsp;&nbsp;
                    <select class="form-control" name="merchant" id="merchant">
                        <option value="">Select a Merchant</option>
                        @foreach($merchant as $mer)
                        <option value="{{$mer->name}}">{{$mer->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
      </form>
      </div>
</div>
<div class="panel panel-default" id="report" style="display: none;">
    <div class="panel-heading master-order" style=" background-color: #337ab7; background: #337ab7; color: #fff;">
         <h3>Bill Report For Merchat</h3>
         <div style="float: right; position: relative; margin-top: -35px;">
             <form method="post" target="_blank" action="download.php">
                 <input type="hidden" name="merchant_name" style="color:black;" id="merchant_name">
                 <input type="submit" id="submit" class="btn btn-info" value="Save as CSV">
                 &nbsp;&nbsp;
             <button class="btn btn-success" type="button" onclick="printDiv()">Print to printer</button></div>
                 </form>
            <!---<button class="btn btn-info" type="button" onclick="javascript:csv()">Save as CSV</button>-->
            
        
    </div>
  <div class="panel-body">
      
      <div class="row" id="print">
          <form method="post" id="frm_paymet">
          <table class="table" id="csv">
    <thead>
        <!--<tr><th colspan="12" style="background-color:#C5E0B4; background-image: url(header.png); background-size: 100%; border-right:solid 2px; overflow:hidden;" height="200">
&nbsp;</th></tr>-->
        <tr><td colspan="2"><b>Merchant Name</b></td><td colspan="10"><b><u><label id="mer"></label><input type="hidden" id="mer" name="mer"></u></b></td></tr>
    <tr>
        <th>Check</th>
        <th>serial#</th>
        <th>Order NO</oh>
        <th>Date Received</oh>
        <th>Date Delivered</oh>
        <th>SKU</oh>
        <th>Name</oh>
        <th>Qty</oh>
        <th>Unit Price</oh>
        <th>Total Price</oh>
        <th>CN</oh>
        <th>Commission</oh>
        <th>M Payble</oh>
    </tr>
    </thead>
    <tbody id="result">
        
        
    </tbody>
    <tr><td colspan="10" class="text-right">&nbsp;</td><td><b></b></td><td colspan="1">
            <input type="button" name="submit" id="make_payment" class="btn btn-info btn-xs" value="Make Payment"></td></tr>
        </table>
        </form>
      </div>
      
  </div>
</div>

<meta name="_token" content="{!! csrf_token() !!}" />
@stop