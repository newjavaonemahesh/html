@extends('dashboard2')
@section('content')

<div class="panel panel-primary">
  <div class="panel-heading master-order" id='{{$order->order_id}}'>
       <h3 class="panel-title">{{$order->order_id}} | <a href="/merchants/{{$order->merchant}}">{{$order->merchant}}</a></h3>
       <span class="pull-right">
                        <!-- Tabs -->
             <ul class="nav panel-tabs">
                 <li class="active user"><a href="#order-status-tab" data-toggle="tab">Order Status</a></li>
                 <li class="Associated"><a href="#order-history-tab" data-toggle="tab">Order History</a></li>
                 <li class="account"><a href="#order-comments-tab" data-toggle="tab">Comments</a></li>
             </ul>
       </span>
  
  </div>
  <div class="panel-body">
       <div class="tab-content">
           <div class="tab-pane active" id="order-status-tab">
               <table class="table">
                    <thead>
                        <tr>
                        <th>Status</th>
                        <th id="cancelReasonsHeader" hidden="">Cancel Reason</th>
                        <th>Received on</th>
                        <th>Order Amount</th>
                        <th>Order City</th>
                        <th>Courier</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <select id="{{$order->order_id}}" class="orderStatusSelect">
                                    <option value="{{$order->order_status}}">{{$order->order_status}}</option>
                                    <option value="Received">Received</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Forwarded">Forwarded</option>
                                    <option value="Dispatched">Dispatched</option>
                                    <option value="Delivered">Delivered</option>
                                    <option value="Canceled">Canceled</option>
                                    <option value="Returned">Returned</option>
                                </select>
                            </td>
                            <td id="cancelReasons" hidden="">
                                <select id="cancelReasonsSelect">
                                    <option value="0">Select Reason</option>
                                    <option value="1">Out Of Stock</option>
                                    <option value="2">Customer Not Interested</option>
                                    <option value="3">Merchant Not Reachable</option>
                                    <option value="4">Customer Not Reachable</option>
                                    <option value="5">No Coverage Area</option>
                                    <option value="5">Shipping Charges too high</option>
                                    <option value="6">Other</option>
                                </select>
                            </td>
                            <td>{{$order->create_time }}</td>
                            <td>{{$order->grand_amount}}</td>
                            <td>{{$order->city}}</td>
                            <td>{{$order->courier}}</td>
                            <td><button type="button" value="{{$order->order_id}}" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-status-update">Update</button></td>
                        </tr>
                        <tr>
                            <td>Customer confirmed</td><td>@if($order->customer_confirm != null){{$order->customer_confirm}}@else <button type="button" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-customer-confirm">Customer Confirm</button> @endif</td>
                        </tr>
                        <tr>
                            <td>Merchant confirmed</td><td>@if($order->merchant_confirm != null){{$order->merchant_confirm}}@else <button type="button" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-merchant-confirm">Merchant Confirm</button> @endif</td>
                        </tr>
                    </tbody>
                </table>
           </div>
           <div class="tab-pane" id="order-history-tab">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Action</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($logs as $log)
                            <tr>
                                <td>{{$log->user}}({{$log->userEmail}})</td>
                                <td>{{$log->action}}</td>
                                <td>{{$log->created_at}}</td>
                            </tr>
                            @endforeach
                        </tbody>  
                    </table>
           </div>
               
           <div class="tab-pane" id="order-comments-tab">
               <div class="container">
               <table>
                   <thead>
                            <tr>
                                <th>User</th>
                                <th>Time</th>
                                <th>Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($comments as $log)
                            <tr>
                                <td>{{$log->user}}({{$log->userEmail}})</td>
                                <td>{{$log->created_at}}</td>
                                <td>{{$log->comment}}</td>
                            </tr>
                            @endforeach
                        </tbody>  
               </table>
               </div>
               
               <div class="container">
                    <div class="col-md-10">
                        <div class="form-area">  
                            <form role="form">
                                <br style="clear:both">
                                <h6>New Comment</h6>
                                <div class="form-group">
                                    <textarea class="form-control" type="textarea" id="new-comment" placeholder="comment.." maxlength="300" rows="2"></textarea>                    
                                <button type="button" id="{{$order->order_id}}" name="submit" class="btn btn-warning btn-xs pull-right submit-comment">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
           </div>
       </div>
  </div>
      
</div>

<div class="panel panel-default">
  <div class="panel-heading">SKU Details</div>
  <div class="panel-body">
    <table class="table items-table" value="{{$order->merchant}}" id="{{$order->merchant}}">
      <thead>
          <tr>
                <th>SKU</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>List Price</th>
                <th>Sale Price</th>
                <th>Promotion</th>
                <th>Promotion Discount</th>
                <th>Total Price</th>
                <th>Merchant Payable</th>
          </tr>
      </thead>
      <tbody>
          @foreach($items as $item)
          <tr class='item-row'>
              <td class='sku'>{{$item->item_sku}}</td>
              <td class='name'>{{$item->item_name}}</td>
              <td class='quantity'>{{$item->item_quantity}}</td>
              <td class='list-price'>{{$item->item_list_price}}</td>
              <td class='sale-price'>{{$item->item_sale_price}}</td>
              <td class='promotion'>{{{$item->promotion_name or '--'}}}</td>
              <td class='discount'>{{{$item->promotion_discount or '--'}}}</td>
              <td class='grand-price'>{{$item->items_grand_price}}</td>
              <td class='merchant-payable'>{{$item->items_merchant_payable}}</td>
          </tr>
          @endforeach
      </tbody>
      <tfoot>
          <tr>
              <td>
              <button type="button" value="{{$order->order_id}}" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-items-edit">Edit</button>
              </td>
          </tr>
      </tfoot>
    </table>
  </div>
  </div>
<div class="panel panel-default">
  <div class="panel-heading">Shipping Details</div>
  <div class="panel-body">
    <table class="table">
      <thead>
          <tr>
            <th>Courier</th>
            <th>CN</th>
            <th>Destination City</th>
            <th>Forwarded</th>
            <th>Dispatched</th>
            <th>Delivered</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>
                    <a href="#1" id="updateCourier" data-type="select" data-pk="{{$order->order_id}}" data-url="/order/x-editcourier" data-title="Select Courier">{{$order->courier}}</a>
              </td>
              <td><a href="#1" data-pk="{{$order->order_id}}" id="updateCN">@if($order->CN==null) Enter CN @else {{$order->CN}} @endif</a></td>
              <td><a href="#1" data-pk="{{$order->order_id}}" id="updateCity">{{ $order->city }}</a></td>
              <td>@if($order->forward_time !=null) {{$order->forward_time }} @else -- @endif</td>
              <td>@if($order->dispatch_time !=null) {{$order->dispatch_time }} @else -- @endif</td>
              <td>@if($order->deliver_time !=null) {{$order->deliver_time }} @else -- @endif</td>
              <td>
                  @if($order->CN == null) 
                    <button type="button" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-generate-cn">Generate CN</button>
                  @else
                    <button type="button" class="btn btn-warning btn-xs" disabled="">Generate CN</button>
                  @endif
                  @if($order->slip_link != null)
                  <span><a class="btn btn-warning btn-xs" href="{{$order->slip_link}}" target="_blank">Print Slip</a></span>
                  <button type="button" id="{{$order->order_id}}" class="btn btn-warning btn-xs btn-track-cn-removethispart">Track CN</button>
                  @endif
                    
                    
              </td>
          </tr>
          <tr>
              <td><a href="#1" data-type="textarea" data-pk="{{$order->order_id}}" id="updateRemarks">{{$order->shipment_remarks}}</a></td>
          </tr>
      </tbody>
      
    </table>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Customer Details</div>
  <div class="panel-body">
    <table class="table">
      <thead>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Email</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td><a href="#1" id="updateName" data-type="text" data-pk="{{$order->order_id}}">{{$order->name}}</a></td>
              <td><a href="#1" id="updateAddress" data-type="textarea" data-pk="{{$order->order_id}}">{{$order->address}}</a></td>
              <td>{{$order->phone}}</td>
              <td>{{$order->email}}</td>
          </tr>
      </tbody>
      
    </table>
  </div>
</div>
@if($slave !=null)
<div class="panel panel-default">
  <div class="panel-heading">
      <h4 class="panel-title">
          <a data-toggle="collapse" href="#collapse1">+Previous Order</a>
      </h4>
  </div>
  <div class="panel-body collapse" id="collapse1">
    <table class="table">
      <thead>
          <tr>
            <th>Order ID</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>{{$slave->order_id}}</td>
          </tr>
      </tbody>
      
    </table>
  </div>
</div>
@endif
            <!-- End of Table-to-load-the-data Part -->
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 80%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Merchant Items</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmTasks" name="frmTasks" class="form-horizontal" novalidate="">
                                <table class='table item-edit-form'>
                                    <thead>
                                        <tr>
                                            <th>SKU</th>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Sale Price</th>
                                            <th>Merchant Payable</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary btn-merchant-pay" id="btn-save" value="add">Update</button>
                            <input type="hidden" id="task_id" name="task_id" value="0">
                        </div>
                    </div>
                </div>
            </div>


<meta name="_token" content="{!! csrf_token() !!}" />

@stop
