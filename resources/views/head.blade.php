<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Scotch">
<title>Business | Clikky</title>

<link rel="stylesheet" type="text/css" href="/css/bootstrap/css/bootstrap-theme.min.css">
<link rel="stylesheet" type="text/css" href="/css/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/css/bootstrap-editable.css">
<link rel="stylesheet" type="text/css" href="/css/tabs.css">

<script type="text/javascript" src="/js/jquery-2.2.1.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="/js/bootstrap-editable.js"></script>
<script type="text/javascript" src="/js/crm.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

</style>