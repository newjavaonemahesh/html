@extends('dashboard2')
@section('content')


<div class="panel panel-default">
  <div class="panel-heading">Paid Products</div>
  <div class="panel-body">
      <table class="table">
          <thead>
              <tr>
        <th>serial#</th>
        <th>Order NO</oh>
        <th>Date Received</th>
        <th>Date Delivered</th>
        <th>SKU</th>
        <th>Name</th>
        <th>Qty</th>
        <th>Unit Price</th>
        <th>Total Price</th>
        <th>CN</th>
        <th>Commission</th>
        <th>M Payble</th>
              </tr>
          </thead>
          <tbody>
              @foreach($orders as $order)
              <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $order->order_id }}</td>
                <td>{{ $order->create_time }}</td>
                <td>{{ $order->deliver_time }}</td>
                <td>{{ $order->item_sku }}</td>
                <td>{{ $order->item_name }}</td>
                <td>{{ $order->item_quantity }}</td>
                <td>{{ $order->item_sale_price }}</td>
                <td>{{$order->total}}</td>
                <td>{{ $order->CN }}</td>
                <td>{{ $order->commision }}</td>
                <td>{{ $order->items_merchant_payable }}</td>   
              </tr>
              @endforeach
          </tbody>
      </table>
  </div>
  </div>

<meta name="_token" content="{!! csrf_token() !!}" />

@stop
