/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
 * 
 * @param {type} url
 * @param {type} formData
 * @returns {data}
 */
function getPaymentHistory(merchant){
    
}
function runajax(url,formData){
    
    var returnObj;
    
    $.ajaxSetup({
        headers: {
                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })
    var type = "PUT";
    $.ajax({
        async: false,
        type: type,
        url: url,
        data: formData,
        dataType: 'json',
        success: function(data){
        console.log("Success", data);
        returnObj = data;
        },
        error: function(data){
            console.log('Error', data);
            alert("failed");
        }
   });
   return returnObj;
}
 

$(document).ready(function() {
	var url = "/order";
        var commision;
        /*
         * Search Order From Home page
         */
        $("#btn-search-order").click(function(){
            var order_id = $('#search-order-id-text').val();
            var name = $('#search-order-name-text').val();
            var phone = $('#search-order-phone-text').val();
            var status = $('#order-status-dropdown').val();
           var url ="/order";
            var formData = {
              op : "searchOrder",
              id : order_id,
              name: name,
              phone: phone,
              status:status  
            };
            var data = runajax(url,formData);
             console.log("local data:",data);
             var order_id;
             var order_status;
             var receive_date;
             var items_amount;
             var merchant;
             var city;
             var courier;
             var CN;
             var dispatched_time;
             $('#orders_data').empty();
             $.each(data, function(k,v){
                 console.log("in first loop");
                 $.each(v, function(key,value){
                     if(key === 'order_id')
                        order_id=value;
                    if(key==='order_status')
                        order_status=value;
                    if(key==='create_time')
                        receive_date=value;
                    if(key==='items_amount')
                        items_amount=value;
                    if(key==='merchant')
                        merchant=value;
                    if(key==='city')
                        city=value;
                    if(key==='courier')
                        courier=value;
                    if(key==='CN')
                        CN=value;
                    if(key==='dispatch_time')
                        dispatched_time = value;
                 });
                 
                 $('#orders_data').append('<tr><td><a href="/order/'+order_id+'"><span>'+order_id+'</span></a></td>\n\
                                          <td>'+order_status+'</td><td>'+receive_date+'</td><td>'+dispatched_time+'</td><td>'+items_amount+'</td>\n\
                                            <td><a href="/merchants/'+merchant+'"><span>'+merchant+'</span></a></td>\n\
                                            <td>'+city+'</td><td>'+courier+'</td><td>'+CN+'</td></tr>');      
             });
            
        });
        
        /*
         * Search ORder by customer name or phone number
         */
          $("#btn-search-customer").click(function(){
            var name = $('#search-order-name-text').val();
            var phone = $('#search-order-phone-text').val();
           // alert("search order:"+order_id);
           var url ="/order";
            var formData = {
              op : "searchOrderByCustomer",
              name : name,
              phone: phone
            };
            var data = runajax(url,formData);
             console.log("local data:",data);
             var order_id;
             var order_status;
             var receive_date;
             var items_amount;
             var merchant;
             var city;
             var courier;
             var CN;
             var dispatched_time;
             $('#orders_data').empty();
             $.each(data, function(k,v){
                 console.log("in first loop");
                 $.each(v, function(key,value){
                     if(key === 'order_id')
                        order_id=value;
                    if(key==='order_status')
                        order_status=value;
                    if(key==='create_time')
                        receive_date=value;
                    if(key==='items_amount')
                        items_amount=value;
                    if(key==='merchant')
                        merchant=value;
                    if(key==='city')
                        city=value;
                    if(key==='courier')
                        courier=value;
                    if(key==='CN')
                        CN=value;
                    if(key==='dispatch_time')
                        dispatched_time = value;
                 });
                 
                 $('#orders_data').append('<tr><td><a href="/order/'+order_id+'"><span>'+order_id+'</span></a></td>\n\
                                          <td>'+order_status+'</td><td>'+receive_date+'</td><td>'+dispatched_time+'</td><td>'+items_amount+'</td>\n\
                                            <td><a href="/merchants/'+merchant+'"><span>'+merchant+'</span></a></td>\n\
                                            <td>'+city+'</td><td>'+courier+'</td><td>'+CN+'</td></tr>');      
             });
            
        });
        /*
         * 
         */
        $.fn.editable.defaults.ajaxOptions = {type: "PUT"};
        $.fn.editable.defaults.mode = 'inline';
        $.fn.editable.defaults.params = function (params) {
            params._token = $('meta[name="_token"]').attr('content');
            //'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        return params;
        };
        $('#updateCN').editable({
                           type:  'text',
                           name:  'CN',
                           url: '/order/x-editcn',
                           title: 'Enter CN'
         });
        $('#updateCourier').editable({
            value: 4,    
            source: [
              {value: 'Leopard', text: 'Leopard'},
              {value: 'BlueEx', text: 'BlueEx'},
              {value: 'CLIKKY', text: 'CLIKKY'},
              {value: 4, text: 'Select Courier'}
           ]
        });
        $('#updateCity').editable({
                           type:  'text',
                           name:  'City',
                           url: '/order/x-editcity',
                           title: 'City'
         });
         $('#updateName').editable({
                           type:  'text',
                           name:  'Name',
                           url: '/order/x-editname',
                           title: 'Update Name'
         });
         $('#updateAddress').editable({
                           type:  'textarea',
                           name:  'Address',
                           url: '/order/x-editaddress',
                           title: 'Update Address'
         });
         $('#updateRemarks').editable({
                           type:  'textarea',
                           name:  'Remarks',
                           url: '/order/x-editremarks',
                           title: 'Shipping Remarks'
         });
                
               /*
                * Merchant Home Page
                * Add merchant in the system if does not exist 
                */
         $(".btn-merchant-add").click(function(){
             var merchantName = $(".merchant-name").val();
             var merchantBrand = $(".merchant-brand").val();
             var merchantEmail = $(".merchant-email").val();
             var merchantPhone = $(".merchant-phone").val();
             var merchantCity = $(".merchant-city").val();
             var merchantCommission = $(".merchant-commission").val();
             var merchantAddress = $(".merchant-address").val();
             //alert(merchantName);
             if(merchantName===""){
                 alert("Please enter merchant name first");
                 return;
             }else if(merchantBrand===""){
                 alert("Please enter merchant brand first");
                 return;
             }else if(merchantEmail===""){
                 alert("Please enter merchant email first");
                 return;
             }else if(merchantPhone===""){
                 alert("Please enter merchant phone first");
                 return;
             }else if(merchantCommission===""){
                 alert("Please enter merchant commission first");
                 return;
             }else if(merchantAddress===""){
                 alert("Please enter merchant address first");
                 return;
             }else if(merchantCity===""){
                 alert("Please enter merchant city first");
                 return;
             }
              $.ajaxSetup({
                
	 		
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                });
                
             var formData = {
                 op: "addMerchant",
                 name: merchantName,
                 brand: merchantBrand,
                 email: merchantEmail,
                 phone: merchantPhone,
                 city: merchantCity,
                 commission: merchantCommission,
                 address: merchantAddress,
             };
                var type = "PUT";
                var url = "/merchant"
                console.log(formData);
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function(data){
        		console.log("Success", data);
                        location.reload();
                        $(".missing-merchant").remove();
        		//alert("Updated successfully");
                    },
                    error: function(data){
        		console.log('Error', data);
        		alert("failed");
                    }
                });
             
         });

                /*
                 * Order Update
                 * order confirmed by merchant confirm action
                 */
         $(".btn-merchant-confirm").click(function(){
             var order_id = $(this).attr("id");
             var url = "/order";
            // $("#test").attr("id")
             var formData = {
                 op: "merchantConfirmOrder",
                 order_id: order_id,
             };
             runajax(url,formData);
             window.location.href ="/order/"+order_id;
             
         });
         /*
          * Order Update
          * order confirmed by customer confirm action
          */
         $(".btn-customer-confirm").click(function(){
             var order_id = $(this).attr("id");
             var url = "/order";
            // $("#test").attr("id")
             var formData = {
                 op: "customerConfirmOrder",
                 order_id: order_id,
             };
             runajax(url,formData);
             window.location.href ="/order/"+order_id;
             
         });
         /*
          * Home Page
          * Get all critical/Serious/Warning orders and load 
          * Get all received/dispatched/forwarded orders and load
          */
         $('a').click(function(){
             
             var operation = $(this).attr('id');
             var href = $(this).attr('href');
             if(href!=="#")return;
             
             var formData = {
                 op: operation
             };
             url = "/order";
             var data = runajax(url,formData);
             console.log("local data:",data);
             var order_id;
             var order_status;
             var receive_date;
             var items_amount;
             var merchant;
             var city;
             var courier;
             var CN;
             var dispatched_time;
             $('#orders_data').empty();
             $.each(data, function(k,v){
                 console.log("in first loop");
                 $.each(v, function(key,value){
                     if(key === 'order_id')
                        order_id=value;
                    if(key==='order_status')
                        order_status=value;
                    if(key==='create_time')
                        receive_date=value;
                    if(key==='items_amount')
                        items_amount=value;
                    if(key==='merchant')
                        merchant=value;
                    if(key==='city')
                        city=value;
                    if(key==='courier')
                        courier=value;
                    if(key==='CN')
                        CN=value;
                    if(key==='dispatch_time')
                        dispatched_time = value;
                 });
                 
                 $('#orders_data').append('<tr><td><a href="/order/'+order_id+'"><span>'+order_id+'</span></a></td>\n\
                                          <td>'+order_status+'</td><td>'+receive_date+'</td><td>'+dispatched_time+'</td><td>'+items_amount+'</td>\n\
                                            <td><a href="/merchants/'+merchant+'"><span>'+merchant+'</span></a></td>\n\
                                            <td>'+city+'</td><td>'+courier+'</td><td>'+CN+'</td></tr>');      
             });
             $('#pagination').hide();
             //console.log(data);
         });

         /*
          * Payments Page
          * Get names of all merchants and load in the select box
          */
         if(window.location.pathname === '/payment') {
             var url = '/merchant';
            $.ajaxSetup({
                
	 		
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                })
                //e.preventDefault();
                //var token = $('meta[name="_token"]').attr('content');
        
                var formData = {
                    op: 'getAllMerchants',
                }
                var type = "PUT";
                console.log(formData);
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function(data){
        		console.log("Success", data.status);
                        $.each(JSON.parse(data.merchants), function(i, val) {
                           $('#merchant-list').append("<option class='merchant-name'>"+val+"</option>");
                           console.log("hello:" + val);
                        });
                    },
                    error: function(data){
        		console.log('Error', data);
        		alert("failed function not working");
                    }
                });
        }       
	 
        /*
         * Payment Page
         * Retrieve payable orders for a merchant a selected merchant from select box
         */
            $('#merchant-list').change(function(e){
             var merchant = $('#merchant-list').val();
             var url = "/merchant";
             
             $('.merchant-payable-items').empty();
             $.ajaxSetup({
	 		
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                })
                e.preventDefault();
                //var token = $('meta[name="_token"]').attr('content');
        
                var formData = {
                    op: 'getMerchantPayables',
                    name: merchant,
                }
                var type = "PUT";
                console.log(formData);
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function(data){
        		console.log("Success", data);
                        var total_revenue = 0;
                        $.each(data,function(key,value){
                            var order_id;
                            var receive_date;
                            var deliver_date;
                            var items_amount;
                            var items_payable;
                            var order_object;
                            $.each(value, function(k,v){
                                if(k==='order_id')
                                    order_id=v;
                                if(k==='received_date')
                                    receive_date = v;
                                if(k==='delivered_date')
                                    deliver_date = v;
                                if(k==='items_amount')
                                    items_amount = v;
                                if(k==='payable'){
                                    items_payable = v;
                                    total_revenue += items_payable;
                                }
                                    
                            });
                            
                            $('.merchant-payable-items').append("<tr><td><a href='/order/"+order_id+"'><span>"+order_id+"</span></a></td><td>"+receive_date+"</td>\n\
                                <td>"+deliver_date+"</td><td>"+items_amount+"</td><td>"+items_payable+"</td></tr>");
                           /* 
                            var sku;
                            var sku_name;
                            var sku_sale_price;
                            var sku_grand_price;
                            var sku_quantity;
                            $.each(value,function(k,v){
                                if(k==='sku_details'){
                                    $.each(v,function(id,val){
                                        if(id==='sku'){
                                            sku = val;
                                        }
                                        if(id==='name'){
                                            sku_name=val;
                                        }
                                        if(id==='sale_price'){
                                            sku_sale_price=val;
                                        }
                                        if(id==='quantity'){
                                            sku_quantity = val;
                                        }
                                        if(id==='grand_price'){
                                            sku_grand_price=val;
                                        }
                                    });      
                                }
                        });
                        $('.merchant-payable-items').append("<tr><td colspan='1'></td><td>"+sku+"</td>\n\
                                <td>"+sku_name+"</td><td>"+sku_sale_price+"</td><td>"+sku_grand_price+"</td></tr>");
                            */
                        });
                        $('.merchant-payable-items').append("<tr><td>Total Payable</td><td>"+total_revenue+"</td></tr>\n\
                            <tr><td><button type='button' class='btn btn-warning btn-xs btn-make-payment'>Make Payment</button></td></tr>");
                        
                        
                    },
                    error: function(data){
        		console.log('Error', data);
        		alert("failed");
                    }
                });
                
                
             
         });
         
         /*
         * Payment Page
         * Make Payment to the Merchant on payable orders
         */
        $('.merchant-payable-items').on('click', '.btn-make-payment', function(e){
            var merchant = $('#merchant-list').val();
            var url = "/order";
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                })
                e.preventDefault();
                //var token = $('meta[name="_token"]').attr('content');
        
                var formData = {
                    op: 'makeMerchantPayment',
                    name: merchant,
                }
                var type = "PUT";
                console.log(formData);
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function(data){
        		console.log("Success", data);
                    },
                    error: function(data){
        		console.log('Error', data);
        		alert("failed");
                    }
                });
        });
        $('.submit-comment').click(function(e){
            var comment = $('#new-comment').val();
            var order_id = $(this).attr('id');
            var url = "/order";
            
            //alert("Submit comment:"+comment + order_id);
            console.log("order id:"+order_id);
            $.ajaxSetup({
	 		
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                });
            e.preventDefault();
                var formData = {
                    op: "enterNewComment",
                    id: order_id,
                    comment: comment
                }
                var type = "PUT";
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function(data){
        		console.log("Success", data);
        		alert("Updated successfully");
                    },
                    error: function(data){
        		console.log('Error', data);
        		alert("failed");
                    }
                });    
            
        });
        
        $( ".orderStatusSelect" ).change(function() {
            
            var new_status = $('.orderStatusSelect').val();
            if(new_status === 'Canceled'){
                $('#cancelReasons').show();
                $('#cancelReasonsHeader').show();
            }else{
                $('#cancelReasons').hide();
                $('#cancelReasonsHeader').hide();
            }
           });
        /*
          * Order Details page
          * Function to update status of the order. 
          */
         
	 $('.btn-status-update').click(function(e) {
	 	
	 	var order_id = $(this).val();
	 	var new_status = $('select#' + order_id).val();
                var cancel_reason = -1;
	 	console.log(new_status);
	 	console.log(order_id);
                
                if(new_status === 'Canceled'){
                    var cancel_reason = $('#cancelReasonsSelect').val();
                }
                if(cancel_reason === '0'){
                     alert("Provide Cancel Reason");
                     return;
                }
               
               
	 	/*
	 	$.get(url+ '/' + order_id, function(){
	 		console.log("success");
	 	})*/
	 	$.ajaxSetup({
	 		
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
                })
                e.preventDefault();
                //var token = $('meta[name="_token"]').attr('content');
        
                var formData = {
                    op: "updateOrderStatus",
                    task: new_status,
                    id: order_id,
                    cancelReason: cancel_reason
                }
                var type = "PUT";
                console.log(formData);
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function(data){
        		console.log("Success", data);
        		//alert(data.status);
                        location.reload();
                    },
                    error: function(data){
        		console.log('Error', data);
        		alert("failed");
                    }
                });
	 });
         /*
          * Function to generate Courier CN
         */
         $('.btn-generate-cn').click(function(e){
            var url = "/courier";
            var order_id = $(this).attr('id');
            var formData = {
                 op: "generateCN",
                 order_id: order_id
             };
             
            var data = runajax(url,formData);
            console.log("Result:"+data);
            if(data.result==="failed"){
                alert("Failed: "+data.reason);
            }else{
                location.reload();
            }
            
            //window.location.href = "/generatecn/"+order_id;
            
         });
         /*
          * Function to Track Courier CN
         */
         $('.btn-track-cn').click(function(e){
            var url = "/courier";
            var order_id = $(this).attr('id');
            var formData = {
                 op: "trackCN",
                 order_id: order_id
             };
             
            var data = runajax(url,formData);
            console.log("Result:"+data);
            if(data.result==="failed"){
                alert("Failed: "+data.reason);
            }else{
                alert(data.data);
                location.reload();
            }
            
            //window.location.href = "/generatecn/"+order_id;
            
         });
         
         /*
          * Function to Edit the merchant payment values for specific SKUs
          * get commission for the merchant
          */
         
         $('.btn-items-edit').click(function(e){
            //find out merchant name by getting id of table displaying items details
            var merchant = $('.items-table').attr('id');
            
            //Get merchant commission value and suggest merchant payables in the form
            var url = '/merchant';
            $.ajaxSetup({
	 		
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                     }
            })
            e.preventDefault();
            var formData ={
                    merchant: merchant,
                    op: 'getCommision',
                }
                var type = "PUT";
                //console.log(formData);
                $.ajax({
                    type: type,
                    url: url,
                    data: formData,
                    dataType: 'json',
                    success: function(data){
        		console.log("Success", data);
                        commision = data.commision;
                        $("table.item-edit-form tbody").empty(); 
                        $( ".items-table tbody tr.item-row" ).each(function(i,row ) {
                            var $row = $(row);
                            var sale_price = $row.find(".sale-price").text();
                            var quantity = $row.find(".quantity").text();
                            var merchant_payable = quantity*(sale_price * (100 - commision)) / 100;   
                        $("table.item-edit-form tbody").append("<tr><td class='sku'>"+$row.find((".sku")).text()+"</td><td class='name'>\n\
                            "+$row.find((".name")).text()+"</td><td class='quantity'>"+$row.find((".quantity")).text()+"</td><td class='sale-price'>\n\
                            "+$row.find((".sale-price")).text()+"</td><td><input type='text' class='form-control has-error merchant-pay' id='merchant-pay' name='merchant-pay' value='"+
                             merchant_payable+"'></td></tr>");
                            console.log( "sku: " + $row.find((".sku")).text() + "name: " + $row.find((".name")).text());
                        });
                        console.log("comm:",commision);
                        },
                    error: function(data){
        		console.log('Error', data);
        		alert("Merchant not found. Contact administrator");
                    }
                });
           // $('#btn-save').val("add");
            $('#frmTasks').trigger("reset");
            $('#myModal').modal('show');
        });
         
         /*
         * Function to update merchant payables in the database
          */
          $('.btn-merchant-pay').click(function(e){
                //get all rows in the form
                $('table.item-edit-form tbody tr').each(function(i,row){
                    var $row = $(row);
                    //var order_id = $('.master-order').text();
                    var order_id = $('.master-order').attr('id');
                    var sku = $row.find(".sku").text();;
                    var quantity = $row.find(".quantity").text();;
                    var payable_per_sku = $row.find(".merchant-pay").val();
                    var merchant_pay = payable_per_sku;
                    console.log("merchant pay::"+merchant_pay);
                    console.log("pay per sku::"+payable_per_sku);
                    console.log("sku::"+sku);
                    console.log("quantity::"+quantity);
                    console.log("order_id::"+order_id);
                    var url = "/merchant";
                    $.ajaxSetup({
	 		
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
                    e.preventDefault();
                    var formData ={
                        op: 'updateMerchantPayable',
                        order_id: order_id,
                        sku: sku,
                        pay: merchant_pay,      
                    }
                    var type = "PUT";
                    console.log(formData);
                    $.ajax({
                        async: false,
                        type: type,
                        url: url,
                        data: formData,
                        dataType: 'json',
                        success: function(data){    
                        console.log("success:",data);
                        //location.reload();
                        //alert("Updated Successfully");
                        },
                        error: function(data){
        		console.log('Error', data);
                            alert("Unknown Error. Contact administrator");
                        }
                });
                });
                $("#task").remove();
                $('#myModal').modal('hide');
                location.reload();
          });
});
