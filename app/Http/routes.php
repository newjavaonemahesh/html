<?php


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use Illuminate\Http\Request;


Route::group(['middleware' => 'web'], function() {
    Route::auth();
    //Route::get('/register', 'HomeController@index');
    //Route::get('/register', ['middleware' => 'auth', 'uses' => 'HomeController@index']);
    Route::get('/home', 'HomeController@index');
    Route::get('/', ['middleware' => 'auth', 'uses' => 'MainController@index']);
    Route::get('/reports', ['middleware' => 'auth', 'uses' => 'ReportController@index']);
    //UMER
    Route::get('/pay_reports', ['middleware' => 'auth', 'uses' => 'ReportController@view']);
    
    Route::get('/paid_reports', ['middleware' => 'auth', 'uses' => 'ReportController@view_paid_merchant']);
    
    Route::get('/merchant_report', ['middleware' => 'auth', 'uses' => 'ReportController@view_merchant']);
    
    Route::get('/merchant_report_paid', ['middleware' => 'auth', 'uses' => 'ReportController@view_merchant_paid']);
    
    Route::get('/merchant_report_download', ['middleware' => 'auth', 'uses' => 'ReportController@view_merchant_download']);
    
    Route::get('/make_payment', ['middleware' => 'auth', 'uses' => 'ReportController@make_payment']);
    
    Route::get('/unmake_payment', ['middleware' => 'auth', 'uses' => 'ReportController@unmake_payment']);
    ////
    Route::get('/order/{id}', ['middleware' => 'auth', 'uses' => 'MainController@order']);
    
    Route::get('/paid/{id}', ['middleware' => 'auth', 'uses' => 'MainController@paid']);
    
    Route::put('/order', ['middleware' => 'auth', 'uses' => 'MainController@ajax']);
    Route::put('/order/x-editcn', ['middleware' => 'auth', 'uses' => 'MainController@xeditcn']);
    Route::put('/order/x-editcourier', ['middleware' => 'auth', 'uses' => 'MainController@xeditcourier']);
    Route::put('/order/x-editcity', ['middleware' => 'auth', 'uses' => 'MainController@xeditcity']);
    Route::put('/order/x-editname', ['middleware' => 'auth', 'uses' => 'MainController@xeditname']);
    Route::put('/order/x-editaddress', ['middleware' => 'auth', 'uses' => 'MainController@xeditaddress']);
    Route::get('/generatecn/{id}',['middleware' => 'auth', 'uses' => 'MainController@generatecn']);
    Route::get('/merchants/{id?}',['middleware' => 'auth', 'uses' => 'MerchantController@index']);
    Route::put('/merchant', ['middleware' => 'auth', 'uses' => 'MerchantController@ajax']);
    Route::get('/payment', ['middleware' => 'auth', 'uses' => 'PaymentController@index']);
    Route::get('/test', ['middleware' => 'auth', 'uses' => 'TestController@index']);
    Route::get('/upload', ['middleware' => 'auth', 'uses' => 'FileUploadController@index']);
    Route::post('/upload', ['middleware' => 'auth', 'uses' => 'FileUploadController@upload']);
    Route::put('/courier', ['middleware' => 'auth', 'uses' => 'CourierController@ajax']);
    Route::get('/courier/track', ['middleware' => 'auth', 'uses' => 'CourierController@tracktest']);
    Route::get('/test', ['middleware' => 'auth', 'uses' => 'TestController@user']);

});


