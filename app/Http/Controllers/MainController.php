<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

//use App\Http\Requests\Request;

use App\Order;
use App\Order_item;
use App\Edited_order;
use App\Edited_order_item;
use App\Merchant;
use App\Payment;
use \DB;
use \Auth;
use App\Orderlog;
use App\Comment;
use Session;
//use Symfony\Component\Console\Tests\Input;


class MainController extends Controller
{
    var $user;
    public $statusValidation = array("Received"=>"","Forwarded"=>"Received,Pending,Canceled,Confirmed","Dispatched"=>"Forwarded",
        "Delivered"=>"Dispatched","Returned"=>"Delivered,Dispatched","Canceled"=>"Received,Forwarded,Pending","Pending"=>"Received,Forwarded",
        "Confirmed"=>"Pending,Canceled");
    
    public function __construct() {
        $this->user = Auth::user();
        
    }
    
    public function index(){
    	
        $total_orders = Order::all()->count();
        $orders = Order::orderBy('create_time','desc')->paginate(40);
        //all orders whose status is received
        $critical = date('Y-m-d h:m:s', strtotime('-7 days'));
        $serious = date('Y-m-d h:m:s', strtotime('-6 days'));
        $warning = date('Y-m-d h:m:s', strtotime('-4 days'));
        $critical_orders = Order::where('order_status','!=','Delivered')->where('order_status','!=','Canceled')->where('order_status','!=','Returned')->where('create_time', '<', $critical)->get()->count();
        $serious_orders = Order::where('order_status','!=','Delivered')->where('order_status','!=','Canceled')->where('create_time', '>', $critical)->where('create_time', '<', $serious)->get()->count();
        $warning_orders = Order::where('order_status','!=','Delivered')->where('order_status','!=','Canceled')->where('create_time', '>', $serious)->where('create_time', '<', $warning)->get()->count();
        $waiting_confirm = Order::where(function($query){
            $query->where('customer_confirm',null)->orwhere('merchant_confirm',null);
        })->count();

        //Received
        $received_orders = Order::where('order_status','Received')->get()->count();
        $forwarded_orders = Order::where('order_status','Forwarded')->get()->count();
        $dispatched_orders = Order::where('order_status','Dispatched')->get()->count();
        $pending_orders = Order::where('order_status','Pending')->get()->count();
        $completed_orders = Order::where('order_status','Delivered')->count();
        $canceled_orders = Order::where('order_status','Canceled')->count();
        $confirmed_orders = Order::where('order_status','Confirmed')->count();
// Orders by their state
     
     
    	$data = array('orders' => $orders,'all'=> $total_orders ,'critical'=>$critical_orders,'serious'=>$serious_orders,
            'warning'=>$warning_orders,'received'=>$received_orders,'forwarded'=>$forwarded_orders,'dispatched'=>$dispatched_orders,
            'pending'=>$pending_orders,'confirmed'=>$confirmed_orders,'completed'=>$completed_orders,
            'canceled'=>$canceled_orders);
    	return view('content', $data);
    }
    ///////////////////////////////
    //////////umer chattha////////
    
    public function orderall($id){
	$order = Order::where('order_id', $id)->first();
	$items = Order_item::where('order_id', $id)->get();
        $logs = Orderlog::where('order_id',$id)->orderBy('created_at','desc')->get();
        $comments = Comment::where('order_id',$id)->orderBy('created_at','desc')->get();
        $slave_order=null;
        if($order->slave_order_id){
            $slave_order = Edited_order::where('order_id',$order->slave_order_id)->first();
        }
	$data = array('order'=> $order, 'items'=>$items, 'slave'=>$slave_order,"logs"=>$logs,"comments"=>$comments);
	return $data;
    }
    
    ///////////////////////////////
    ///////////////////////////////
    public function order($id){
	$order = Order::where('order_id', $id)->first();
	$items = Order_item::where('order_id', $id)->get();
        $logs = Orderlog::where('order_id',$id)->orderBy('created_at','desc')->get();
        $comments = Comment::where('order_id',$id)->orderBy('created_at','desc')->get();
        $slave_order=null;
        if($order->slave_order_id){
            $slave_order = Edited_order::where('order_id',$order->slave_order_id)->first();
        }
	$data = array('order'=> $order, 'items'=>$items, 'slave'=>$slave_order,"logs"=>$logs,"comments"=>$comments);
	return view('order4',$data);
    }
    public function pro($id){
	
        $data['orders'] = DB::select( DB::raw("SELECT *,o.id as p_id FROM orders as o INNER join order_items as ot ON o.order_id = ot.order_id "
                . " INNER JOIN merchants as m ON o.merchant = m.name WHERE o.merchant_paid = :somevariable "), array(
   'somevariable' => $id,));
	
	return view('pro4',$data);
    }
    public function ajax(Request $request){
        
        if($request->op == "updateOrderStatus"){
            $order_id = $request->id;
            $the_order = Order::where('order_id',$order_id)->first();
            $current_state = $the_order['order_status'];
            $new_state = $request->task;
            $ret_status =0;
            if($new_state=='Forwarded'){
                $forwarded = date("Y-m-d h:m:s");
                $data = array();
                if($this->validateStatus($current_state, $new_state)){
                    if($the_order->customer_confirm == null||$the_order->merchant_confirm ==null ){
                        Session::flash('msg', 'Either customer or merchant has not confirmed this order.');
                        Session::flash('alert_class', 'alert-danger');
                        $ret_status = -1;
                    }else{
                        //Session::flash('msg', 'There has been an error.');
                        //Session::flash('alert_class', 'alert-danger');
                    }
                    if($ret_status == 0){
                        $the_order = Order::where('order_id',$order_id)->update(['order_status'=>$new_state, 'forward_time'=>$forwarded]);
                        $data = ['status'=>'success', 'orderID'=>$request->id, 'previousStatus'=>$current_state ,'newStatus'=>$request->task];
                        $this->writelog("Forwarded", $order_id);
                        Session::flash('msg', 'Status updated');
                        Session::flash('alert_class', 'alert-success');
                    }elseif($ret_status == -1){
                        $data = ['status'=>'Failed'];
                    }
                }else{
                    Session::flash('msg', 'There has been an error.');
                    Session::flash('alert_class', 'alert-danger');
                }
                return Response::json($data);
            
            }elseif($new_state=="Dispatched"){
                $dispatched = date("Y-m-d h:m:s");
                $data = array();
                if($this->validateStatus($current_state, $new_state)){
                    $the_order = Order::where('order_id', $order_id)->update(['order_status'=>$new_state, 'dispatch_time'=>$dispatched]);
                    $data = ['status'=>'success', 'orderID'=>$request->id, 'previousStatus'=>$current_state ,'newStatus'=>$request->task];
                    $this->writelog("Dispatched", $order_id);
                    Session::flash('msg', 'Status updated');
                    Session::flash('alert_class', 'alert-success');
                }else{
                    Session::flash('msg', 'Orders in Forwarded state may be Dispatched');
                    Session::flash('alert_class', 'alert-danger');
                    $data = ['status'=>'Failed'];
                }
                return Response::json($data);
            }elseif($new_state=='Delivered'){
                $delivered = date("Y-m-d h:m:s");
                $data = array();
                if($this->validateStatus($current_state, $new_state)){
                    $the_order = Order::where('order_id', $order_id)->update(['order_status'=>$new_state, 'deliver_time'=>$delivered]);
                    $data = ['status'=>'success', 'orderID'=>$request->id, 'newStatus'=>$request->task];
                    $this->writelog("Delivered", $order_id);
                    Session::flash('msg', 'Status updated');
                    Session::flash('alert_class', 'alert-success');
                }else{
                    Session::flash('msg', 'Orders in Dispatched state may be Delivered');
                    Session::flash('alert_class', 'alert-danger');
                    $data = ['status'=>'Failed'];
                }
                return Response::json($data);
            }elseif($new_state=='Canceled'){
                $data = array();
                if($this->validateStatus($current_state, $new_state)){
                    $the_order = Order::where('order_id', $order_id)->update(['order_status'=>$new_state,'cancel_reason'=>$request->cancelReason]);
                    $data = ['status'=>'success', 'orderID'=>$request->id, 'newStatus'=>$request->task];
                    $this->writelog("Canceled", $order_id);
                    Session::flash('msg', 'Status updated');
                    Session::flash('alert_class', 'alert-success');
                }else{
                    Session::flash('msg', 'Changing to Cancel from current state not allowed.');
                    Session::flash('alert_class', 'alert-danger');
                    $data = ['status'=>'Failed'];
                }
                return Response::json($data);
            }elseif($new_state=='Returned'){
                $data = array();
                if($this->validateStatus($current_state, $new_state)){
                    $the_order = Order::where('order_id', $order_id)->update(['order_status'=>$new_state]);
                    $data = ['status'=>'success', 'orderID'=>$request->id, 'newStatus'=>$request->task];
                    $this->writelog("Canceled", $order_id);
                    Session::flash('msg', 'Status updated');
                    Session::flash('alert_class', 'alert-success');
                }else{
                    Session::flash('msg', 'Orders in Received/Forwarded/Pending may not be Returned');
                    Session::flash('alert_class', 'alert-danger');
                    $data = ['status'=>'Failed'];
                }
                return Response::json($data);
            }elseif($new_state=='Pending'){
                $data = array();
                if($this->validateStatus($current_state, $new_state)){
                    $the_order = Order::where('order_id', $order_id)->update(['order_status'=>$new_state]);
                    $data = ['status'=>'success', 'orderID'=>$request->id, 'newStatus'=>$request->task];
                    $this->writelog("Canceled", $order_id);
                    Session::flash('msg', 'Status updated');
                    Session::flash('alert_class', 'alert-success');
                }else{
                    Session::flash('msg', 'Orders in Received/Forwarded state can be set to Pending');
                    Session::flash('alert_class', 'alert-danger');
                }
                return Response::json($data);
            }else{
                $data = ['status'=>'Error. Nothing changed'];
                Session::flash('msg', 'Not Allowed');
                Session::flash('alert_class', 'alert-danger');
                
                return Response::json($data);
            }
            
        }
        elseif($request->op == "getCriticalOrders"){
            $critical = date('Y-m-d h:m:s', strtotime('-7 days'));
            $critical_orders = Order::where('order_status','!=','Delivered')->where('order_status','!=','Canceled')->where('order_status','!=','Returned')->where('create_time', '<', $critical)->get();
           // $data = json_encode($critical_orders);
            return Response::json($critical_orders);
        }elseif($request->op == "getSeriousOrders"){
            $critical = date('Y-m-d h:m:s', strtotime('-7 days'));
            $serious = date('Y-m-d h:m:s', strtotime('-6 days'));
            $serious_orders = Order::where('order_status','!=','Delivered')->where('order_status','!=','Canceled')->where('create_time', '>', $critical)->where('create_time', '<', $serious)->get();
            return Response::json($serious_orders);
        }elseif($request->op == "getWarningOrders"){
            $serious = date('Y-m-d h:m:s', strtotime('-6 days'));
            $warning = date('Y-m-d h:m:s', strtotime('-4 days'));
            $warning_orders = Order::where('order_status','!=','Delivered')->where('order_status','!=','Canceled')->where('create_time', '>', $serious)->where('create_time', '<', $warning)->get();
            return Response::json($warning_orders);
        }elseif($request->op == "getReceivedOrders"){
            $required_orders = Order::where('order_status','Received')->get();
            return Response::json($required_orders);
        }elseif($request->op == "getForwardedOrders"){
            $required_orders = Order::where('order_status','Forwarded')->get();
            return Response::json($required_orders);
        }elseif($request->op == "getDispatchedOrders"){
            $required_orders = Order::where('order_status','Dispatched')->get();
            return Response::json($required_orders);
        }elseif($request->op == "getPendingOrders"){
            $required_orders = Order::where('order_status','Pending')->get();
            return Response::json($required_orders);
        }elseif($request->op == "getAllOrders"){
            $required_orders = Order::orderBy('created_at','desc')->get();
            return Response::json($required_orders);
        }elseif($request->op == "getWaitingOrders"){
           // $required_orders = Order::where('customer_confirm',null)->orwhere('merchant_confirm',null)-get();
            $required_orders=Order::where('order_status','Confirmed')->get();
            /*
            $required_orders=Order::where(function($query){
                $query->where('customer_confirm',null)->orwhere('merchant_confirm',null);
            })->get();
             * 
             */
            return Response::json($required_orders);
        }elseif($request->op == "getCompletedOrders"){
            $required_orders = Order::where('order_status','Delivered')->get();
            return Response::json($required_orders);
        }elseif($request->op == "getCanceledOrders"){
            $required_orders = Order::where('order_status','Canceled')->get();
            return Response::json($required_orders);
        }elseif($request->op == 'searchOrder'){
            $name = $request->name;
            $phone = $request->phone;
            $status = $request->status;
            $id = $request->id;
            if($status == "PayableNotSet"){
                $order_all = Order::all();
                $order = $order_all->reject(function($tmp){
                    foreach($tmp->items as $item){
                        if($item->items_merchant_payable != NULL)
                            return $tmp;
                    }
                });           
            }else{
               $order = Order::where('order_id',$id)->orwhere('phone',$phone)->orwhere('name',$name)->orwhere('order_status',$status)->get(); 
            }
            return Response::json($order);
        }
        elseif($request->op =="merchantConfirmOrder"){
            $confirmed = date("Y-m-d h:m:s");
            $order_id = $request->order_id;
            $the_order = Order::where('order_id',$order_id)->first();
            $the_order->merchant_confirm = $confirmed;
            //$the_order = Order::where('order_id',$order_id)->update(['merchant_confirm'=>$confirmed]);
            
            //check if customer and merchant has confirmed the order
            if($the_order->customer_confirm != null){
                $the_order->order_status = "Confirmed";
                
            }
            //
            $the_order->save();
            $this->writelog("Merchant confirmation", $order_id);
            $data = ["status"=>"Success"];
            return Response::json($data);
        }elseif($request->op =="customerConfirmOrder"){
            $confirmed = date("Y-m-d h:m:s");
            $order_id = $request->order_id;
            $the_order = Order::where('order_id',$order_id)->first();
            $the_order->customer_confirm = $confirmed;
            
            //check if customer and merchant has confirmed the order
            if($the_order->merchant_confirm != null){
                $the_order->order_status = "Confirmed";   
            }
            $the_order->save();
            $data = ["status"=>"Success"];
            $this->writelog("Customer confirmation", $order_id);
            return Response::json($data);
        }elseif($request->op=="enterNewComment"){
            $order_id = $request->id;
            $comment = $request->comment;
            $this->writeComment($comment, $order_id);
            return Response::json(array("status"=>"success"));
            
        }
        elseif($request->op =="generateCN"){
            $id = $request->order_id;
            $order = Order::where('order_id', $id)->first();
            $courier = $order->courier;
            if($order->CN != null){
                $data = ["status"=>"Failed","reason"=>"CN exists"];
                return Response::json($data);
            }
            switch ($courier){
                case "BlueEx":
                    break;
                case "Leopard":
                    $this->generateLeopardCN($id);
                    break;    
            }
            $this->writelog("CN generated", $order_id);
        }
        elseif($request->op == "makeMerchantPayment"){
            $merchant = $request->name;
            $payable_orders = Order::where('merchant',$merchant)->where('merchant_paid',NULL)->where('order_status','Delivered')->get();
            $grand_payable=0;
            foreach($payable_orders as $order){
                //echo "hello";
                $order_payable_amount = 0;
                foreach($order->items as $item){
                    //if merchant payable is not updated fail the operation
                    if($item->items_merchant_payable == null){
                        return Response::json(["status"=>"failed","reason"=>"merchant payable not set for an order"]);
                    }else{
                        $order_payable_amount+=$item->items_merchant_payable;
                    }
                }
                $grand_payable+=$order_payable_amount;
            }
            $merchant_brand = Merchant::where('name',$merchant)->select('merchantBrand')->first();
            $payment = new Payment;
            $payment->merchantName = $merchant;
            $payment->merchantBrand = $merchant_brand['merchantBrand'];
            $payment->amount = $grand_payable;
            $payment->save();
            $payment_id = $payment->id;
            
            foreach($payable_orders as $order){
                //update the payment status of each order
                $order->merchant_paid = $payment_id;
                $order->save();
            }
            
            $data = ["status"=>"success","merchant"=>$request->name,"paymentid"=>$payment_id];
            return Response::json($data);
        }
    }
    public function xeditcn(Request $request){
        $orderId = $request->input('pk');
    
        $newCN = $request->input('value');
        $order = Order::where('order_id',$orderId)->first();
        $order->CN = $newCN;
        $order->save();
        $this->writelog("CN update", $orderId);
    }
    public function xeditcourier(Request $request){
        $orderId = $request->input('pk');
    
        $newCourier = $request->input('value');
        $order = Order::where('order_id',$orderId)->first();
        $order->courier = $newCourier;
        $order->save();
        $this->writelog("Courier update", $orderId);
    }
    public function xeditcity(Request $request){
        $orderId = $request->input('pk');
    
        $newCity = $request->input('value');
        $order = Order::where('order_id',$orderId)->first();
        $order->city = $newCity;
        $order->save();
        $this->writelog("Order city updated", $orderId);
    }
    public function xeditname(Request $request){
        $orderId = $request->input('pk');
    
        $newName = $request->input('value');
        $order = Order::where('order_id',$orderId)->first();
        $order->name = $newName;
        $order->save();
        $this->writelog("Customer name updated", $orderId);
    }
    public function xeditaddress(Request $request){
        $orderId = $request->input('pk');
    
        $newAddress = $request->input('value');
        $order = Order::where('order_id',$orderId)->first();
        $order->address = $newAddress;
        $order->save();
        $this->writelog("Customer address updated", $orderId);
    }
    public function xeditremarks(Request $request){
        $orderId = $request->input('pk');
    
        $remarks = $request->input('value');
        $order = Order::where('order_id',$orderId)->first();
        $order->shipment_remarks = $remarks;
        $order->save();
        $this->writelog("Remarks:".$remarks, $orderId);
    }
    public function writelog($action,$order_id){
        $log = new Orderlog();
        $log->user = $this->user->name;
        $log->userEmail = $this->user->email;
        $log->action = $action;
        $log->order_id = $order_id;
        $log->save();
    }
    public function writeComment($com, $order_id){
        //echo "comment:" . $com . " order id:" . $order_id;
        $comment = new Comment();
        $comment->order_id = $order_id;
        $comment->user = $this->user->name;
        $comment->userEmail = $this->user->email;
        $comment->comment = $com;
        $comment->save();
    }
    public function validateStatus($current,$wanted){
        $allowedStatesRaw = $this->statusValidation[$wanted];
        $allowedStates = explode(',', $allowedStatesRaw);
        foreach($allowedStates as $status){
            if($status == $current){
                return true;
            }
        }
        return false;
    }
}
