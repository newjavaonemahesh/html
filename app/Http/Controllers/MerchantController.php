<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\Order_item;
use App\Merchant;
use Response;
use Session;

class MerchantController extends Controller {
    public function index($id=null){
        $createForm = false;
        if($id!=null){
           $merchant = Merchant::where('name',$id)->first();
           if($merchant == null){
               $createForm = true;
           }
           $orders = Order::where('merchant', $id)->get();
           $sales = 0;
           $revenue = 0;
           $paymentDue =0;
           $costOfSales=0;
           foreach($orders as $order){
               foreach($order->items as $item){
                   $sales +=$item->item_sale_price;
                   if($order->order_status == 'Delivered'){
                       $revenue +=$item->item_sale_price;
                       $paymentDue += $item->items_merchant_payable;
                   }
               }  
           }
           
           $delivered = $orders->reject(function($orders_com){
               return $orders_com->order_status != "Delivered";
           }) ;
           $canceled = $orders->reject(function($orders_can){
               return $orders_can->order_status != "Canceled";
           });
           $dashboard = array('orders'=>$orders->count(),'delivered'=>$delivered->count(),'canceled'=>$canceled->count()
                   ,'revenue'=>$revenue,'sales'=>$sales,'paymentDue'=>$paymentDue);
           
           $data = array('name'=>$id,'orders'=>$orders, 'dashboard'=>$dashboard,'createForm'=>$createForm );
           return view('merchant',$data);
        }
        
        echo "Error: Merchant Not Fount";
      
    }
    
    public function ajax(Request $request){
        $operation = $request->op;
        if($operation=='getCommision'){ 
            $merchant = Merchant::where('name',$request->merchant)->first();
            $data = ['status'=>'success', 'commision'=>$merchant->commision];
            return Response::json($data);
        }elseif($operation=='updateMerchantPayable'){
            //Update the payable for each SKU against the order
            $order_entry = Order_item::where('order_id',$request->order_id)->where('item_sku',$request->sku)->first();
            $order_entry->items_merchant_payable = $request->pay;
            $order_entry->save();
            $data = ['status'=>'success. Payment detail updated','order-id'=>$order_entry->order_id,'sku'=>$order_entry->item_sku,'merch-pay'=>$order_entry->items_merchant_payable];
            return Response::json($data);
        }elseif($operation=='getAllMerchants'){
            $merchants = Merchant::lists('name');
            $data1 = json_encode($merchants);
            $data = ['status'=>'success', 'merchants'=>$data1];
            return Response::json($data);
        }elseif($operation=='getMerchantPayables'){
            $merchant = $request->name;
            $payable_orders = Order::where('merchant',$merchant)->where('merchant_paid',NULL)->where('order_status','Delivered')->get();
            //$data = ['status'=>'success. getMer', 'merchants'=>$merchant, 'Payable Orders'=>$payable_orders->count() ];
            $result_tmp;
            $data = null;
            foreach($payable_orders as $order){
                //echo "hello";
                $payable_amount = 0;
                $result_tmp = null;
                
                $result_tmp['order_id'] = $order->order_id;
                $result_tmp['received_date'] = $order->create_time;
                $result_tmp['delivered_date'] = $order->deliver_time;
                $result_tmp['items_amount'] = $order->items_amount;
                foreach($order->items as $item){
                    $sku_details = array();
                    $payable_amount+=$item->items_merchant_payable;
                    $sku_details['sku'] = $item->item_sku;
                    $sku_details['name'] = $item->item_name;
                    $sku_details['quantity'] = $item->item_quantity;
                    $sku_details['sale_price']=$item->item_sale_price;
                    $sku_details['grand_price'] = $item->items_grand_price;
                    $result_tmp['sku_details'] = $sku_details;
                }
                $result_tmp['payable'] = $payable_amount;
                $data[$order->order_id] = $result_tmp;
            }
            
            if($data!=null){
               return Response::json($data);
            }else{
                return Response::json(null);
            }
            
            
        }elseif($operation == "addMerchant"){
            $name = $request->name;
            $brand = $request->brand;
            $phone = $request->phone;
            $email = $request->email;
            $address = $request->address;
            $commission = $request->commission;
            $city = $request->city;
            $new_merchant = new Merchant;
            $new_merchant->name = $name;
            $new_merchant->merchantBrand = $brand;
            $new_merchant->phone = $phone;
            $new_merchant->email = $email;
            $new_merchant->address = $address;
            $new_merchant->city = $city;
            $new_merchant->commision = $commission;
            try{
                $new_merchant->save();
                Session::flash('msg', 'Merchant data added successfully.');
                Session::flash('alert_class', 'alert-success');
            } catch (Exception $ex) {
                Session::flash('msg', 'There has been an error.');
                Session::flash('alert_class', 'alert-danger');
            }
            
            $data = ["status"=>"success"];
            return Response::json($data);
        }else{
            $data = ['status'=>'failed', 'message'=>'No matched operation found'];
            return Response::json($data);
        }
    }

}
