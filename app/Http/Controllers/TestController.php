<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\Order_item;
use App\Merchant;
use \Auth;

class TestController extends Controller
{
    //
    public function index(){
            $blueex = new BlueEx();
            $courier = "bluex";
            $order_id = "CLK100893";
            //Retrieve order information
            $order = Order::where('order_id', $order_id)->first();
            //Retrieve items information
            $items = Order_item::where('order_id',$order_id)->get();
            $shipperDetails = ["name"=>$order->name,"address"=>$order->address,"city"=>$order->city,"phone"=>$order->phone];
            $merchant_name = $order->merchant;
            $merchant = Merchant::where('name',$merchant_name)->first();
            $merchant_blueID = $merchant->blueID;
            echo "merchant id: " . $merchant_blueID . "<br>";
            if($merchant_blueID == null){
                echo "Running bluex account create ...." . "<br>";
                $merchant_blueID = $blueex->create_merchant($merchant);
                $merchant->blueID = $merchant_blueID;
                $merchant->save();
            }
            
            
            $result = $blueex->cn_generate($items,$shipperDetails,$merchant_blueID);
            $order->cn = $result->cn;
            $order->courier = $courier;
            $order->save();
            $updated_order = Order::where('order_id', $order_id)->first();
            $order_details = array('order'=> $updated_order, 'items'=>$items);
    }
    
    public function user(){
        $user = Auth::user();
        echo $user->name;
        //print_r($user);
    }
}
