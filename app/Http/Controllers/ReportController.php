<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\Order_item;
use Illuminate\Database\Eloquent\Collection;
use \DB;
use Gate;
class ReportController extends Controller
{
    //
    public function index(){
        
        if(Gate::denies('viewReports')){
            return view("notauthorized");
        }
        $day_of_month = date('d');
        $ref_date = date('Y-m-01 00:00:00');
        $orders = Order::all();
        // Canceled Orders Data
        $canceled_orders = $orders->where('order_status','Canceled');
        $can_report['oos'] = round((Order::where('cancel_reason','1')->count()/$canceled_orders->count())*100,2);
        $can_report['cni'] = round((Order::where('cancel_reason','2')->count()/$canceled_orders->count())*100,2);
        $can_report['mnr'] = round((Order::where('cancel_reason','3')->count()/$canceled_orders->count())*100,2);
        $can_report['cnr'] = round((Order::where('cancel_reason','4')->count()/$canceled_orders->count())*100,2);
        $can_report['nca'] = round((Order::where('cancel_reason','5')->count()/$canceled_orders->count())*100,2);
        $can_report['sth'] = round((Order::where('cancel_reason','6')->count()/$canceled_orders->count())*100,2);
        $can_report['other'] = round((Order::where('cancel_reason','7')->orwhere('cancel_reason',null)->count()/$canceled_orders->count())*100,2);
        
        $can_report_m['total'] = Order::where('order_status','Canceled')->where('create_time','>',$ref_date)->count();
        $can_report_m['oos'] = round((Order::where('cancel_reason','1')->where('create_time','>',$ref_date)->count()/$can_report_m['total'])*100,2);
        $can_report_m['cni'] = round((Order::where('cancel_reason','2')->where('create_time','>',$ref_date)->count()/$can_report_m['total'])*100,2);
        $can_report_m['mnr'] = round((Order::where('cancel_reason','3')->where('create_time','>',$ref_date)->count()/$can_report_m['total'])*100,2);
        $can_report_m['cnr'] = round((Order::where('cancel_reason','4')->where('create_time','>',$ref_date)->count()/$can_report_m['total'])*100,2);
        $can_report_m['nca'] = round((Order::where('cancel_reason','5')->where('create_time','>',$ref_date)->count()/$can_report_m['total'])*100,2);
        $can_report_m['sth'] = round((Order::where('cancel_reason','6')->where('create_time','>',$ref_date)->count()/$can_report_m['total'])*100,2);
        $can_report_m['other'] = round((Order::where('cancel_reason','7')->orwhere('cancel_reason',null)->where('create_time','>',$ref_date)->count()/$can_report_m['total'])*100,2);
        /***********/
        $completed_orders= $orders->where('order_status','Delivered');
        $transit_orders = $orders->where('order_status','Dispatched');
        $pending_orders = $orders->where('order_status','Pending');
        
        $revenue=0;
        $cost=0;
        $GMV=0;
        //Calculate GMV
        foreach($orders as $order){
            $GMV += $order->grand_amount;
        }
        $n = number_format($GMV);
        //Calculating 1) Revenue 2) Costs
        foreach($completed_orders as $order){
            $order_amount = $order->items_amount;
            $order_shipping_charges = $order->shipping_charges;
            $order_shipping_cost = $order->courier_cost;
            $order_cost = $order_shipping_cost - $order_shipping_charges; 
            $merchant_sku_payables = 0;
            foreach($order->items as $item){
                $merchant_sku_payables+=$item->items_merchant_payable;
            }
            $order_revenue = $order_amount - $merchant_sku_payables;
            $revenue+=$order_revenue;
            $cost+=$order_cost;
        }
        
        /* Average Basked Size - Start
         * Basket Size = Amount of money in the order
         * Average Basket Size = Avg amount of money in each order
         */
        $total_orders = $orders->count();
        $ABS = round($GMV/$total_orders,0);
        //
        /*
         * Avg Item Size
         */
        $order_items = Order_item::all();
        $unique_skus = $order_items->count();
        $AIS = round($GMV/$unique_skus,0);
        /*
         * Avg Order Per Day e.g AOPD
         */
        
        $month_orders = Order::where('create_time','>',$ref_date)->count();
        $AOPD = round($month_orders/$day_of_month,2);
        $data = array("orders"=>$orders,'completed'=>$completed_orders,'canceled'=>$canceled_orders,'forwarded'=>$transit_orders,'pending'=>$pending_orders
                ,"revenue"=>$revenue,"cost"=>$cost,"GMV"=>$GMV,"ABS"=>$ABS,"AIS"=>$AIS,"AOPD"=>$AOPD,'cities'=>array(),'can_report'=>$can_report,
            'can_report_m'=>$can_report_m);
        /*
         * City vise order report
         */
        
       // $cities = Order::select('city')->groupBy('city')->get();
        $cities = Order::select('city')->distinct()->orderBy('city','ASC')->get();
       // print_r($cities);
        //echo "Total Cities:" . $cities->count() . "<br>";
        foreach($cities as $key){ 
           // $count = $orders->where('city','like',$key->city)->count();
            $count = $order->where('city', $key->city)->count();
            $percentage = round(($count/$total_orders)*100,2);
            $city['name'] = $key->city;
            $city['orders'] = $count;
            $city['percentage'] = $percentage;
            array_push($data['cities'], $city);
        }
        return view("reports",$data);
    }
    ///umer chattha
    public function view()
    {
        $merchant['merchant'] = DB::table('merchants')->select('name')->get();
        return view("pay_report",$merchant);
    }
    public function view_paid_merchant()
    {
        $merchant['merchant'] = DB::table('merchants')->select('name')->get();
        return view("paid_report",$merchant);
    }
    public function view_merchant()
    {
        $merchant_name =  $_GET['merchant'];
        //$order['order'] = DB::table('orders')->where('merchant', '=', $merchant_name )->get();
        $orders = DB::select( DB::raw("SELECT *,o.id as p_id FROM orders as o INNER join order_items as ot ON o.order_id = ot.order_id "
                . " INNER JOIN merchants as m ON o.merchant = m.name WHERE o.merchant = :somevariable AND o.order_status = :w2nd  "
                . "AND o.merchant_paid IS NULL order by o.order_id desc"), array(
   'somevariable' => $merchant_name,
   'w2nd' => 'Delivered',
 ));
       // $order = DB::select( DB::raw("SELECT * FROM orders where merchant = " . $merchant_name));
        $gtotal = 0;
     ?>
        $order_id_color = '';
        <?php $i = 1; foreach ($orders as $order)
            { 
            ?>
    <tr style="">
        <td><input type="checkbox" name="order_ids[]" value="<?php echo $order->p_id; ?>" checked="checked"/>
            <input type="checkbox" name="order_payable[]" value="<?php echo $order->items_merchant_payable; ?>" checked="checked" />
        </td>
        <td><?php echo $i++;  ?></td>
        <td><?php echo $order->order_id; ?></td>
        <td><?php echo substr($order->create_time, 0,10); ?></td>
        <td><?php echo substr($order->deliver_time, 0,10); ?></td>
        <td><?php echo $order->item_sku; ?></td>
        <td><?php echo $order->item_name; ?></td>
        <td><?php echo $order->item_quantity; ?></td>
        <td><?php echo $order->item_sale_price; ?></td>
        <td><?php $total = $order->item_quantity * $order->item_sale_price;  echo $total;  ?></td>
        <td><?php echo $order->CN; ?></td>
        <td><?php echo $order->commision; ?></td>
        <td><?php  $payable2 =  $order->items_merchant_payable; echo $payable2; ?>
            
        </td>
    </tr>
        <?php 
        $gtotal = $gtotal + $payable2;
        }
        ?>
    <tr><td colspan="10" class="text-right">&nbsp;</td><td><b>Total</b></td><td colspan="1"><b><?php echo $gtotal; ?></b>
            <input type="hidden" name="amount" value="<?php echo $gtotal; ?>" /></td></tr>
     <?php
    }
    
    public function view_merchant_paid()
    {
        $merchant_name =  $_GET['merchant'];
        //$order['order'] = DB::table('orders')->where('merchant', '=', $merchant_name )->get();
        $orders = DB::select( DB::raw("Select * from payments where merchantName = :merchantName"), array(
   'merchantName' => $merchant_name,
 ));
       // $order = DB::select( DB::raw("SELECT * FROM orders where merchant = " . $merchant_name));
        $gtotal = 0;
     ?>
        <?php $i = 1; foreach ($orders as $order)
            { 
            ?>
        <tr style="">
        <td><input type="checkbox" name="order_ids[]" value="<?php echo $order->id; ?>" checked="checked"/><?php echo $order->id; ?></td>
        <td><a href="pro/<?php echo $order->id; ?>"><?php echo "See Items"; ?></a></td>
        <td><?php echo $order->created_at; ?></td>
        <td><?php echo $order->updated_at; ?></td>
        <td><?php echo $order->merchantName; ?></td>
        <td><?php echo $order->amount;  ?></td>
        
    </tr>
        <?php
        $gtotal = $gtotal + $order->amount;
        }
        ?>
    <tr><td  class="text-right">&nbsp;</td><td><b>Total paid amount</b></td><td colspan="1"><b><?php echo $gtotal; ?></b></td></tr>
     <?php
    }
    
     public function make_payment()
     {
         extract($_GET);
         //echo 'merchant name = ' .$mer;
         //echo $amount.'amount';
         $number = implode(',',$order_ids);
                 $gtotal ='';
                 foreach($order_payable as $total)
                 {
                     $gtotal = $gtotal + $total;
                 }
                 $amount1 =  $gtotal;
         //echo $number;
         $date = date('Y-m-d');
         $check = DB::table('payments')->insert(['created_at' => $date, 'updated_at' => $date , 'merchantName' => $mer , 'amount' => $amount1]);
         if($check)
         {
             //echo 'submit';
             $last_id = DB::getPdo()->lastInsertId();
             
             //echo $last_id;
             foreach($order_ids as $id)
             {
                 $update = DB::table('orders')->where('id', $id)->update(['merchant_paid' => $last_id]);
             if($update)
             {
                echo 'done';
             }
             else 
                 {
                 echo 'no';
                 
                 }
             }
             
         }
 else { echo 'not';}

         
     }
     public function unmake_payment()
     {
         extract($_GET);
         //echo 'merchant name = ' .$mer;
         //echo $amount.'amount';
         foreach($order_ids as $id)
         {
         
         $check = DB::table('orders')->where('merchant_paid', $id)->update(['merchant_paid' => NULL]);
         if($check)
         {
             DB::table('payments')->where('id', $id)->delete();
             echo 'done';
         }
        else
            { 
            echo 'not';
            }
     }
     }

     
     
 }   
