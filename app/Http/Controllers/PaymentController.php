<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Order;
use App\Order_item;
use App\Merchant;
use Response;
use App\Payment;

class PaymentController extends Controller{
    public function index(){
        $payments = Payment::all()->sortByDesc('id');
        $data = array('payments'=>$payments);
        return view('payment',$data);
    }
}