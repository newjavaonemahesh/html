<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests;
use Request;
use App\Order;
use App\Order_item;
use App\Merchant;
use App\Payment;
use DateTime;
use \DB;

class FileUploadController extends Controller
{
    //
    var $newOrders= array();
    var $editedOrders= array();
    var $movedToHistory = array();
    var $duplicatOrders = array();
    var $skipped = array();
    var $verbose = true;
    public function index(){
        return view('fileupload');
    }
    
    public function upload(){
        $records=0;
        $file = Request::file('filefield');
        $extension = $file->getClientOriginalExtension();
	Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
        $csvData = \Excel::load($file)->get();
        foreach($csvData as $row){
            $records +=1; //read each row of the excel file
            if(! $this->check_order_id($row->order_id)){ //returns true if order exists
                //check if order is an edited order. If there is a - in the order ID then its edited
                $order_has_previous = explode("-", $row->order_id);
                /////Handle Edited - start
                if(sizeof($order_has_previous)>1){
                    //$new_order_id=$row->order_id;
                    $original_order_id = implode("-",  array_slice($order_has_previous, 0, sizeof($order_has_previous)-1));
                    if($this->verbose){
                        echo 'This order looks like edited. "<br>"';
                        echo 'current id =' . $row->order_id . '<br>' ;
                        echo 'previous id =' . $original_order_id . '<br>' ;
                    }
                    
                    if($this->check_order_id($original_order_id)){
                        //echo $original_order_id . " exits. Moving to history <br>";
                        //Fetch existing order ID so some of its parameters can be reused with new order
                        $orginal_order = Order::where('order_id',$original_order_id)->first();
                        $this->move_order_to_history($original_order_id, $row->order_id);
                        array_push($this->movedToHistory,$original_order_id);
                        array_push($this->editedOrders,$row->order_id);
                        //
                        $this->create_order($row,$orginal_order);
                        array_push($this->newOrders,$row->order_id);
                    }else{
                        //echo "Previous ID: " . $original_order_id . " does not exist<br>";
                        array_push($this->newOrders,$row->order_id);
                        //echo "Creating " . $row->order_id . "<br>";
                        $this->create_order($row);
                    }
                    ///Handle Edited - finish
                }else{
                    //array_push($newOrders,$row->order_id);
                    $this->create_order($row);
                    array_push($this->newOrders,$row->order_id);
                }
                
            }else{
                //Order ID from excel already exists
                array_push($this->duplicatOrders,$row->order_id);
            }
        }
        $data = array("total_records"=>$records,'duplicate'=>$this->duplicatOrders,'new'=>$this->newOrders,'edited'=>$this->editedOrders,'archived'=>$this->movedToHistory,
            'skipped'=>$this->skipped);
        return view('fileupload',$data);
    }
    
    public function create_order($row,$original_order=null){
        $order = new Order;
        $order->order_id = $row->order_id;
          
        $order->create_time = date('Y-m-d H:i:s',strtotime($row->date_created));
        $order->name = ucfirst($row->customer_name);
        $order->order_status = "Received";
        $order->address = $row->address;
        $order->city = ucfirst($row->city);
        //echo "Email ID::" . $row->Email . "<br>";
        $order->email_id = $row->Email;
        $order->phone = $row->phone;
        $order->items_amount = $row->total_price;
        $order->shipping_charges = $row->shipping_price;
        $order->grand_amount = $row->grand_total;
        $order->merchant =null;
        if($original_order!=null){
            $order->slave_order_id = $original_order->order_id;
            $order->order_status = $original_order->order_status;
            $order->forward_time = $original_order->forward_time;
            $order->customer_confirm = $original_order->customer_confirm;
            $order->merchant_confirm = $original_order->merchant_confirm;
            $order->CN = $original_order->CN;
            $order->courier = $original_order->courier;
        }
        //$ordered_items = $row->items;
        $item_details_raw =  $row->items_details;
       // echo "SKU Details " . $item_details_raw . "<br>";
         $ordered_item_skus = explode("|",$item_details_raw);
         //echo "SKUs in the order: " . sizeof($ordered_item_skus) . "<br>";
         for($i=0;$i<sizeof($ordered_item_skus);$i++){
             
             $tmp_array = explode(";",$ordered_item_skus[$i]);
             if(sizeof($tmp_array)>10){
                 $items = new Order_Item;
                //echo "loading sku...". $tmp_array[1] ."<br>";
                $order->merchant = $tmp_array[0];
                $items->order_id = $row->order_id;
                $items->item_name = $tmp_array[2];
                $items->item_sku = $tmp_array[1];
                $items->item_quantity = $tmp_array[7];
                $items->item_sale_price = $tmp_array[8];
                $items->items_grand_price = $tmp_array[11];
                try{
                    $order->save();
                    $items->save();
                } catch (\Exception $ex) {
                    echo "Error loading....". $order->order_id . "<br>";
                    array_push($this->skipped,$row->order_id);
                    echo $ex->getMessage();
                    echo "<br>";
                }
            }else{
                array_push($this->skipped,$row->order_id);
                //echo "Skiped..." . $order->order_id . "<br>";
                }   
            }
    }
    public function move_order_to_history($original_order_id,$new_order_id){
        echo "Try moving order-".$original_order_id. " to history <br>";
        $order = Order::where('order_id',$original_order_id)->first();
        $order_items = Order_item::where('order_id',$original_order_id)->get();
        $edited_order = new \App\Edited_order;
        $edited_order_item = new \App\Edited_order_item;
        if($order!=null){
            echo "Moving order-".$original_order_id. " to history <br>";
            $edited_order->order_id = $order->order_id;
            $edited_order->name = $order->name;
            $edited_order->address = $order->address;
            $edited_order->city = $order->city;
            $edited_order->create_time = $order->create_time;
            $edited_order->merchant = $order->merchant;
            $edited_order->items_amount = $order->items_amount;
            $edited_order->shipping_charges = $order->shipping_charges;
            $edited_order->grand_amount = $order->grand_amount;
            $edited_order->master_order_id = $new_order_id;
            $edited_order->save();
            $order->delete();
            foreach($order_items as $item){
                $edited_order_item->order_id = $item->order_id;
                $edited_order_item->promotion_name = $item->promotion_name;
                $edited_order_item->promotion_discount = $item->promotion_discount;
                $edited_order_item->item_name = $item->item_name;
                $edited_order_item->item_sku = $item->item_sku;
                $edited_order_item->item_quantity = $item->item_quantity;
                $edited_order_item->item_category = $item->item_category;
                $edited_order_item->item_list_price = $item->item_list_price;
                $edited_order_item->item_sale_price = $item->item_sale_price;
                $edited_order_item->items_grand_price = $item->item_grand_price;
                $edited_order_item->save();
                $item->delete();
            }
        }else{
            echo $original_order_id. " does not exist. consider new order as original<br>";
        }
    }
    public function check_order_id($id){
        $order = Order::where('order_id',$id)->exists();
        if($order)
            return true;
        else
            return false;
    }
}
