<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

//use App\Http\Requests\Request;

use App\Order;
use App\Order_item;
use App\Edited_order;
use App\Edited_order_item;
use App\Merchant;
use App\Payment;
use \DB;
//use Symfony\Component\Console\Tests\Input;


class CourierController extends Controller
{
    var $L_key = "9AC987DFA7EA38223045325F887A4392";
    var $L_pass = "TIOFLAK2016";
    public function ajax(Request $request){
        $operation = $request->op;
        $id = $request->order_id;
        $order = Order::where('order_id',$id)->first();
        $courier = $order->courier;
        switch($operation){
            case "trackCN":
                switch ($courier) {
                    case "Leopard":
                        $result = $this->trackCNLeopard($order);
                        return Response::json(array("status"=>"success","data"=>$result));
                        break;
                    default:
                        break;
                }
                break;
            case "generateCN":
                switch ($courier) {
                    case "Leopard":
                        $status = $this->leopard($order);
                        if($status==1){
                            return Response::json(array("result"=>"success"));
                        }elseif($status==-1){
                            return Response::json(array("result"=>"failed","reason"=>"Orgin or destination city not correct"));
                        }else{
                            return Response::json(array("result"=>"failed","reason"=>$status));
                        }
                        break;
                    case "BlueEx":
                        $this->blueEx($order);
                        break;
                    default:
                        return Response::json(array("status"=>"failed","reason"=>"no valid courier to the order"));
                        break;
                }
       }
    }
    public function leopard($order){
        //$order = Order::where('order_id',$id)->first();
        $merchant = Merchant::where('name',$order->merchant)->first();
        $d_city = $this->getCitiesLeopard(ucfirst($order->city));
        $o_city = $this->getCitiesLeopard(ucfirst($merchant->city));
        
        if($d_city == -1||$o_city == -1){
            return -1;
        }
        
        $info = array("o_city"=>$o_city,"d_city"=>$d_city,"merchant"=>$merchant->name,"merchant_address"=>$merchant->address,
            "customer_name"=>$order->name,"customer_address"=>$order->address,"customer_phone"=>$order->phone,
            "amount"=>$order->grand_amount,"order_id"=>$order->order_id,"remarks"=>$order->shipment_remarks);
        
        $output = $this->bookCNLeopard($info);
        $status = $output['status'];
        $CN = $output['track_number'];
        $slip_link = $output['slip_link'];
        if($status==1){
            $order->CN = $CN;
            $order->slip_link = $slip_link;
            $order->save();
            //$data = array("status"=>"success","CN"=>$CN);
            //return Response::json($data);
        }
        return $status;
        
    }
    
    public function getCitiesLeopard($req_city){
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, 'http://www.leopardscod.com/webservice/getAllCities/format/json/'); // Write here Test or Production Link
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
        'api_key' => $this->L_key,
        'api_password' => $this->L_pass
        ));

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        //print_r($buffer);
        $decoded_json = json_decode($buffer, true);
        $cities = $decoded_json['city_list'];
        //$req_city = "Rawalpindi";
        $req_id = -1;
        foreach($cities as $city) { 
             if($city['name']==$req_city){
                $req_id = $city['id']; 
             }
        }
        //echo "CITY ID is: " . $req_id;
        return $req_id;
    }
    
    public function trackCNLeopard($info){
        $curl_handle = curl_init();

        // For Direct Link Access use below commented link
        //curl_setopt($curl_handle, CURLOPT_URL, 'http://www.leopardscod.com/webservice/trackBookedPacket/?api_key=XXXX&api_password=XXXX&track_numbers=XXXXXXXX');  // For Get Mother/Direct Link

        curl_setopt($curl_handle, CURLOPT_URL, 'http://www.leopardscod.com/webservice/trackBookedPacket/format/json/');  // Write here Test or Production Link
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
            'api_key' => $this->L_key,
            'api_password' => $this->L_pass,
            'track_numbers' => $info['CN']                      // E.g. 'XXYYYYYYYY' OR 'XXYYYYYYYY,XXYYYYYYYY,XXYYYYYY' 10 Digits each number
        ));

          $buffer = curl_exec($curl_handle);
          curl_close($curl_handle);
          echo $buffer;
          return json_decode($buffer,true);

    }
    public function bookCNLeopard($info){
        $curl_handle = curl_init();

            // For Direct Link Access use below commented link
            //curl_setopt($curl_handle, CURLOPT_URL, 'http://www.leopardscod.com/webservice/trackBookedPacket/?api_key=XXXX&api_password=XXXX&track_numbers=XXXXXXXX');  // For Get Mother/Direct Link

                curl_setopt($curl_handle, CURLOPT_URL, 'http://www.leopardscod.com/webservice/bookPacket/format/json/');  // Write here Test or Production Link
                curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle, CURLOPT_POST, 1);
                    curl_setopt($curl_handle, CURLOPT_POSTFIELDS, array(
                'api_key' => $this->L_key,
                'api_password' => $this->L_pass,
                'booked_packet_weight' => 500,                    // Weight should in 'Grams' e.g. '2000'
                'booked_packet_vol_weight_w' => 0,              // Optional Field (You can keep it empty), Volumetric Weight Width
                'booked_packet_vol_weight_h' => 0,              // Optional Field (You can keep it empty), Volumetric Weight Height
                'booked_packet_vol_weight_l' => 0,              // Optional Field (You can keep it empty), Volumetric Weight Length
                'booked_packet_no_piece' => 1,                  // No. of Pieces should an Integer Value
                'booked_packet_collect_amount' => $info['amount'],            // Collection Amount on Delivery
                'booked_packet_order_id' => $info['order_id'],             // Optional Filed, (If any) Order ID of Given Product
    
                'origin_city' => $info['o_city'],                        // Params: 'self' or 'integer_value' e.g. 'origin_city' => 'self' or 'origin_city' => 789 (where 789 is Lahore ID)
                                                      // If 'self' is used then Your City ID will be used.
                                                      // 'integer_value' provide integer value (for integer values read 'Get All Cities' api documentation)
                'destination_city' => $info['d_city'],                   // Params: 'self' or 'integer_value' e.g. 'destination_city' => 'self' or 'destination_city' => 789 (where 789 is Lahore ID)
                                                      // If 'self' is used then Your City ID will be used.
                                                      // 'integer_value' provide integer value (for integer values read 'Get All Cities' api documentation)
    
                'shipment_name_eng' => $info['merchant'],                  // Params: 'self' or 'Type any other Name here', If 'self' will used then Your Company's Name will be Used here
                'shipment_email' => 'support@clikky.com',                     // Params: 'self' or 'Type any other Email here', If 'self' will used then Your Company's Email will be Used here
                'shipment_phone' => 'self',                        // Params: 'self' or 'Type any other Phone Number here', If 'self' will used then Your Company's Phone Number will be Used here
                'shipment_address' => $info['merchant_address'],                   // Params: 'self' or 'Type any other Address here', If 'self' will used then Your Company's Address will be Used here
                'consignment_name_eng' => $info['customer_name'],               // Type Consignee Name here
                'consignment_email' => 'support@clikky.com',                  // Optional Field (You can keep it empty), Type Consignee Email here
                'consignment_phone' => $info['customer_phone'],                     // Type Consignee Phone Number here
                'consignment_phone_two' => '',                 // Optional Field (You can keep it empty), Type Consignee Second Phone Number here
                'consignment_phone_three' => '',               // Optional Field (You can keep it empty), Type Consignee Third Phone Number here
                'consignment_address' => $info['customer_address'],                // Type Consignee Address here
                'special_instructions' => $info['remarks'],               // Type any instruction here regarding booked packet
                ));

            $buffer = curl_exec($curl_handle);
            curl_close($curl_handle);
            return json_decode($buffer,true);
    }
    
    public function tracktest(){
        $order = Order::where('order_id','CLK122952-1')->first();
        $courier = $order->courier;
        $operation = "trackCN";
        switch($operation){
            case "trackCN":
                switch ($courier) {
                    case "Leopard":
                        $result = $this->trackCNLeopard($order);
                        return Response::json(array("status"=>"success","data"=>$result));
                        break;
                    default:
                        break;
                }
                break;
            case "generateCN":
                switch ($courier) {
                    case "Leopard":
                        $status = $this->leopard($order);
                        if($status==1){
                            return Response::json(array("result"=>"success"));
                        }elseif($status==-1){
                            return Response::json(array("result"=>"failed","reason"=>"Orgin or destination city not correct"));
                        }else{
                            return Response::json(array("result"=>"failed","reason"=>$status));
                        }
                        break;
                    case "BlueEx":
                        $this->blueEx($order);
                        break;
                    default:
                        return Response::json(array("status"=>"failed","reason"=>"no valid courier to the order"));
                        break;
                }
       }
    }
}

