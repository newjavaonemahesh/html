<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function items()
    {
        return $this->hasMany('App\Order_item','order_id','order_id');
    }
    public function logs(){
        return $this->hasMany('App\Orderlog','order_id','order_id');
    }
    public function comments(){
        return $this->hasMany('App\Comment','order_id','order_id');
    }
}
