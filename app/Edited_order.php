<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edited_order extends Model
{
    public function edited_items()
    {
        return $this->hasMany('App\Edited_order_item','order_id','order_id');
    }
}
